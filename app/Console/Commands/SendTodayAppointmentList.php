<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\CronController;

class SendTodayAppointmentList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //command: php artisan sendtodayappointmentlist
    protected $signature = 'sendtodayappointmentlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to send todays appointment list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronController $cron_controller)
    {
        parent::__construct();
        $this->cron_controller = $cron_controller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->cron_controller->SendTodayAppointmentList();
      $this->line("Todays Appointment List Send Successfully.");  
    }
}
