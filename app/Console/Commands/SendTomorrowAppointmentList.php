<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\CronController;

class SendTomorrowAppointmentList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //command: php artisan sendtomorrowappointmentlist
    protected $signature = 'sendtomorrowappointmentlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to send tomorrows appointment list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronController $cron_controller)
    {
        parent::__construct();
        $this->cron_controller = $cron_controller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->cron_controller->SendTomorrowAppointmentList();
      $this->line("Tomorrows Appointment List Send Successfully.");  
    }
}
