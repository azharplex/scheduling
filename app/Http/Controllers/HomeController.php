<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Log;
use Auth;
use Session;
use Validator;
use DB;
use Input;
use Excel;
use Mail;

use App\UserEmail;
use App\EmailSetting;

class HomeController extends Controller
{
  public function index()
  {
    if(Auth::check())
    {    
      Session::forget('menu');
      return redirect()->to('/admin/dashboard');      
    }
    else
    {
      return view('welcome');
    }    
  }
  
  public function sendMessage($contact_numbers, $message, $title){
    // $address = "";  
    for($i=0; $i<count($contact_numbers); $i++)
    {  
      $number = trim($contact_numbers[$i]);
      Log::info('Send message to: ' .$number);
    //   $address .= '<ADDRESS TO="'.$number.'"></ADDRESS>'
		$url = "https://control.msg91.com/api/v5/flow/";
		$send_data = json_decode($message, true); 
		$send_data['mobiles'] = '+91'.$number;
		$send_data = json_encode($send_data , true);
		// dd($send_data,$number);
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'accept: application/json',
			'authkey: 206242As8nKOiTzeXx5aba3a76',
			'content-type: application/json',
		]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
		if (curl_errno($ch)) {
			Log::error('cURL Error: ' . curl_error($ch));
		}
		curl_close($ch);
		Log::info('Response Data: ' .$response);
		echo $response;
			// if($i==1){
			// 	break;
			// };
    }
    //$url = "http://login.startmessaging.in/api/postsms.php";  
    // $url = "http://sms.startmessaging.in/api/postsms.php";  
    // $xml = '<MESSAGE><AUTHKEY>206242As8nKOiTzeXx5aba3a76</AUTHKEY>';
    // $xml .= '<SENDER>KRSTPL</SENDER>';
    // $xml .= '<ROUTE>4</ROUTE>';
    // $xml .= '<CAMPAIGN>PrasadLad</CAMPAIGN>';
    // $xml .= '<COUNTRY>91</COUNTRY>';
    // $xml .= '<UNICODE>1</UNICODE>';
    // $xml .= '<SMS TEXT="'.urlencode($message).'">'.$address.'</SMS>';
    // $xml .= '</MESSAGE>';
    // $curl = curl_init();
    // curl_setopt( $curl, CURLOPT_URL, $url );
    // curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST" );
    // curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt( $curl, CURLOPT_POSTFIELDS, $xml );
    // $response_data = curl_exec( $curl );
    // curl_close( $curl );
	// 	Log::info('Response Data: ' .$response_data);
    // return $response_data;
  }

  public function getSMSBalance()
  {
    $curl = curl_init();    
    //$get_balance = "http://login.startmessaging.in/api/balance.php?authkey=206242As8nKOiTzeXx5aba3a76&type=4"; 
    $get_balance = "http://sms.startmessaging.in/api/balance.php?authkey=206242As8nKOiTzeXx5aba3a76&type=4"; 
    curl_setopt( $curl, CURLOPT_URL, $get_balance );
    curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "GET" );
    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
    $sms_balance = json_decode(curl_exec( $curl ));
    curl_close( $curl );
    return $sms_balance;
  }
  
  //Send SMS 
  public function getSendSMS(Request $request){
    return view('admin.send_sms')->with('menu', 'send_sms')->with('sub_menu', 'send_sms')->with('sms_balance', $this->getSMSBalance());
  }
    
  //Post import excel
  public function postSendSMS(Request $request)
  { 
    $request_data = $request->all();
    
    $messages = [
      'excel-file.required' => 'Please select excel file name.'
    ];
    
    $validator = Validator::make($request_data, [
      'excel-file' => 'required'
    ], $messages);
    
    if($request['excel-file'] == null || $request['excel-file'] == "")
    {
      return redirect()->back()
      ->withInput()
      ->withErrors([
        'excel-error' 			=> 'Please select a excel file',
      ]);
    }
    
    if($validator->fails())
    { 
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {
      if(Input::hasFile('excel-file')){
        $path = Input::file('excel-file')->getRealPath();
        $extension = Input::file('excel-file')->getClientOriginalExtension(); 
        if(strtolower($extension) != 'xls' && strtolower($extension) != 'xlsx'){
          //if file is not excel
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please upload a valid file',
          ]);
        }
        $data = Excel::load($path, function($reader) {})->get();
        $totalRows = $data->count();
        if($totalRows > 1500)
        {
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please import maximum 1500 contact numbers',
          ]);
        }
        foreach($data as $row){
          if(isset($row['contact_number']) && $row['contact_number'] != null && $row['contact_number'] != "" && strlen($row['contact_number']) == 10) {   
            $full_name = $row['full_name'];   
            $contact_number = $row['contact_number']; 
            if($full_name == "")
            {
              $full_name = '';
            }   
            $contacts[] = $contact_number;           
            $contactnames[] = $full_name;           
          }
          else
          {
						continue;
            /*return redirect()->back()
              ->withInput()
              ->withErrors([
              'excel-error' 			=> 'Please enter correct contact number',
            ]);*/
          }
        }
				
				if(count($contacts) <= 0)
				{
					return redirect()->back()
						->withInput()
						->withErrors([
						'excel-error' 			=> 'Please enter correct contact number',
					]);
				}
				
        if(count($contacts) <= 1500)
        {
          return view('admin.send_appointment_sms')->with('menu', 'send_appointment_sms')->with('contacts', json_encode($contacts))->with('contactnames', json_encode($contactnames))->with('sms_balance', $this->getSMSBalance())->with('total_contacts', count($contacts));
        }
        else
        {
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please import maximum 1500 contact numbers',
          ]);
        }
      }
      else{
        return redirect()->back()
        ->withInput()
        ->withErrors([
          'excel-error' 			=> 'Please select a excel file',
        ]);
      }
    }
  }  
  
  //Send sms to user
  public function postSendSMSText(Request $request)
  {
    $request_data = $request->all();
   
    $messages = [
      'title.required' => 'Please enter title',
      'message.required' => 'Please enter SMS content',
      'contacts.required' => 'Please import contact',
    ];
    
    $validator = Validator::make($request_data, [
			//'title' => 'required',
			'message' => 'required',
			'contacts' => 'required',
		], $messages);
    
    if($validator->fails())
    {
        return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {  
      if(isset($request_data['contacts'])){
        $contact_numbers = json_decode($request_data['contacts']);
        $contact_numbers = array_values(array_unique($contact_numbers));
        $message = $request_data['message'];
        $message = nl2br($message);
        $message = str_replace('<br />', '\n', $message);
        $title = 'Prasad Lad';
        $this->sendMessage($contact_numbers, $message, $title);
        return redirect()->back()->with('success', 'Messages Send Successfully');
      }   
    }
  }
	
	public function getSubscribers(){
		if(auth()->check()){
			$curl = curl_init();
			$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
			$group_array = $members_array = [];
			$list_id = $group_id = 0;
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_POSTFIELDS => '',
				CURLOPT_HTTPHEADER => array(
					'Authorization: apikey '.$key
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			if ($err) {
				//echo "cURL Error #:" . $err;
			} else {
				if($response !== false) {
					$data = json_decode($response);
					if(isset($data->lists)){
						foreach($data->lists as $list) {
							$list_id = $list->id;
							break;
						}
						
						if($list_id !== 0) {
							curl_setopt_array($curl, array(
								CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/interest-categories',
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => '',
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 30,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => 'GET',
								CURLOPT_POSTFIELDS => '',
							));

							$group_response = curl_exec($curl);
							$err = curl_error($curl);

							if ($err) {
								echo 'cURL Error #:' . $err;
							} else {
								if($group_response !== false){
									$group_response = json_decode($group_response);
									if(isset($group_response->categories)){
										foreach($group_response->categories as $group) {
											if($group->title == 'Groups') {
												$group_id = $group->id;
											}
										}
									}
								}
								
								if($group_id !== 0) {
									
									curl_setopt_array($curl, array(
										CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/interest-categories/'.$group_id.'/interests?count=1000',
										CURLOPT_RETURNTRANSFER => true,
										CURLOPT_ENCODING => '',
										CURLOPT_MAXREDIRS => 10,
										CURLOPT_TIMEOUT => 30,
										CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										CURLOPT_CUSTOMREQUEST => 'GET',
										CURLOPT_POSTFIELDS => ''
									));

									$group_data = curl_exec($curl);
									
									$err = curl_error($curl);

									if ($err) {
										echo "cURL Error #:" . $err;
									} else {
										if($group_data !== false) {
											$group_data = json_decode($group_data);
											if(isset($group_data->interests)){
												foreach($group_data->interests as $interest) {
													$group_array[$interest->id] = $interest->name;
												}
											}
										}
									}
								}
							}
							curl_setopt_array($curl, array(
								CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/members?count=1000000',
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => '',
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 30,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => 'GET',
								CURLOPT_POSTFIELDS => ''
							));

							$member_response = curl_exec($curl);
							$err = curl_error($curl);

							curl_close($curl);

							if ($err) {
								echo "cURL Error #:" . $err;
							} else {
								if($member_response !== false) {
									$member_response = json_decode($member_response);
									if(isset($member_response->members)) {
										foreach($member_response->members as $member) {
											$members_array[$member->email_address]['id'] = $member->id;
											$members_array[$member->email_address]['fname'] = $member->merge_fields->FNAME;
											$members_array[$member->email_address]['lname'] = $member->merge_fields->LNAME;
											$members_array[$member->email_address]['groups'] = (isset($member->interests) ? $member->interests : []);
										}
									}
								}
							}
						}
					}
				}
			}
			return view('admin.subscribers')
						->with('menu', 'send_mail')
						->with('sub_menu', 'subscribers')
						->with('interest_group_id', $group_id)
						->with('list_id', $list_id)
						->with('groups', $group_array)
						->with('members', $members_array)
						->with('sms_balance', $this->getSMSBalance());
		}
		else {
			return redirect('/');
		}
	}
	
	public function getGroups() {
		$subscribers_array = [];
		if(auth()->check()) {
			$curl = curl_init();
			$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
			$group_array = $members_array = [];
			$list_id = $group_id = 0;
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_POSTFIELDS => '',
				CURLOPT_HTTPHEADER => array(
					'Authorization: apikey '.$key
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			if ($err) {
				//echo "cURL Error #:" . $err;
			} else {
				if($response !== false) {
					$data = json_decode($response);
					if(isset($data->lists)){
						foreach($data->lists as $list) {
							$list_id = $list->id;
							break;
						}
						
						if($list_id !== 0) {
							curl_setopt_array($curl, array(
								CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/interest-categories',
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => '',
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 30,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => 'GET',
								CURLOPT_POSTFIELDS => '',
							));

							$group_response = curl_exec($curl);
							$err = curl_error($curl);
							if ($err) {
								echo 'cURL Error #:' . $err;
							} else {
								if($group_response !== false){
									$group_response = json_decode($group_response);
									if(isset($group_response->categories)){
										foreach($group_response->categories as $group) {
											if($group->title == 'Groups') {
												$group_id = $group->id;
											}
										}
									}
								}
								
								if($group_id !== 0) {
									
									curl_setopt_array($curl, array(
										CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/interest-categories/'.$group_id.'/interests?count=1000',
										CURLOPT_RETURNTRANSFER => true,
										CURLOPT_ENCODING => '',
										CURLOPT_MAXREDIRS => 10,
										CURLOPT_TIMEOUT => 30,
										CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										CURLOPT_CUSTOMREQUEST => 'GET',
										CURLOPT_POSTFIELDS => ''
									));

									$group_data = curl_exec($curl);
									
									$err = curl_error($curl);

									if ($err) {
										echo "cURL Error #:" . $err;
									} else {
										if($group_data !== false) {
											$group_data = json_decode($group_data);
											if(isset($group_data->interests)){
												foreach($group_data->interests as $interest) {
													$group_array[$interest->id] = $interest->name;
													
													$subscribers_array[$interest->id] = $interest->subscriber_count;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			return view('admin.groups')
						->with('menu', 'send_mail')
						->with('sub_menu', 'groups')
						->with('interest_group_id', $group_id)
						->with('list_id', $list_id)
						->with('groups', $group_array)
						->with('subscribers', $subscribers_array)
						->with('sms_balance', $this->getSMSBalance());
		}
		else {
			return redirect('/');
		}
	}
	
	public function getEditGroup(Request $request) {
		iF(auth()->check()){
			$request_data = $request->all();
			
			return view('admin.edit_group')
						->with('menu', 'send_mail')
						->with('sub_menu', 'groups')
						->with('interest_group_id', $request_data['group_id'])
						->with('list_id', $request_data['list_id'])
						->with('name', $request_data['name'])
						->with('id', $request_data['id'])
						->with('sms_balance', $this->getSMSBalance());
		}
		else {
			return redirect('/');
		}
	}
	
	public function postAddGroup(Request $request) {
		$request_data = $request->all();
		
		$curl = curl_init();
		$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$request_data['list_id'].'/interest-categories/'.$request_data['group_id'].'/interests',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => '{"name":"'.$request_data['name'].'"}',
			CURLOPT_HTTPHEADER => array(
				'Authorization: apikey '.$key
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			//echo "cURL Error #:" . $err;
		} else {
			//
		}
		return redirect('/groups')->with('success', 'Group added successfully!');
	}
	
	public function getDeleteGroup(Request $request) {
		$request_data = $request->all();
		
		$curl = curl_init();
		$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$request_data['list_id'].'/interest-categories/'.$request_data['group_id'].'/interests/'.$request_data['id'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'DELETE',
			CURLOPT_POSTFIELDS => '',
			CURLOPT_HTTPHEADER => array(
				'Authorization: apikey '.$key
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			//echo "cURL Error #:" . $err;
		} else {
			//
		}
		return redirect('/groups')->with('success', 'Group deleted successfully!');
	}
	
	public function postEditGroup(Request $request) {
		$request_data = $request->all();
		
		$curl = curl_init();
		$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$request_data['list_id'].'/interest-categories/'.$request_data['group_id'].'/interests/'.$request_data['id'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'PATCH',
			CURLOPT_POSTFIELDS => '{"name":"'.$request_data['name'].'"}',
			CURLOPT_HTTPHEADER => array(
				'Authorization: apikey '.$key
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			//echo "cURL Error #:" . $err;
		} else {
			//
		}
		return redirect('/groups')->with('success', 'Group updated successfully!');
	}
	
  public function getSendMail(Request $request){
		if(auth()->check()){
			$curl = curl_init();
			$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
			$group_array = $members_array = [];
			$list_id = $group_id = 0;
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_POSTFIELDS => '',
				CURLOPT_HTTPHEADER => array(
					'Authorization: apikey '.$key
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			if ($err) {
				//echo "cURL Error #:" . $err;
			} else {
				if($response !== false) {
					$data = json_decode($response);
					if(isset($data->lists)){
						foreach($data->lists as $list) {
							$list_id = $list->id;
							break;
						}
						
						if($list_id !== 0) {
							curl_setopt_array($curl, array(
								CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/interest-categories',
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => '',
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 30,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => 'GET',
								CURLOPT_POSTFIELDS => '',
							));

							$group_response = curl_exec($curl);
							$err = curl_error($curl);

							if ($err) {
								echo 'cURL Error #:' . $err;
							} else {
								if($group_response !== false){
									$group_response = json_decode($group_response);
									if(isset($group_response->categories)){
										foreach($group_response->categories as $group) {
											if($group->title == 'Groups') {
												$group_id = $group->id;
											}
										}
									}
								}
								
								if($group_id !== 0) {
									
									curl_setopt_array($curl, array(
										CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/interest-categories/'.$group_id.'/interests?count=1000',
										CURLOPT_RETURNTRANSFER => true,
										CURLOPT_ENCODING => '',
										CURLOPT_MAXREDIRS => 10,
										CURLOPT_TIMEOUT => 30,
										CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										CURLOPT_CUSTOMREQUEST => 'GET',
										CURLOPT_POSTFIELDS => ''
									));

									$group_data = curl_exec($curl);
									
									$err = curl_error($curl);

									if ($err) {
										echo "cURL Error #:" . $err;
									} else {
										if($group_data !== false) {
											$group_data = json_decode($group_data);
											if(isset($group_data->interests)){
												foreach($group_data->interests as $interest) {
													$group_array[$interest->id] = $interest->name;
												}
											}
										}
									}
								}
							}
							curl_setopt_array($curl, array(
								CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/members?count=1000000',
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => '',
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 30,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => 'GET',
								CURLOPT_POSTFIELDS => ''
							));

							$member_response = curl_exec($curl);
							$err = curl_error($curl);

							curl_close($curl);

							if ($err) {
								echo "cURL Error #:" . $err;
							} else {
								if($member_response !== false) {
									$member_response = json_decode($member_response);
									if(isset($member_response->members)) {
										foreach($member_response->members as $member) {
											$members_array[$member->email_address]['id'] = $member->id;
											$members_array[$member->email_address]['fname'] = $member->merge_fields->FNAME;
											$members_array[$member->email_address]['lname'] = $member->merge_fields->LNAME;
											$members_array[$member->email_address]['groups'] = (isset($member->interests) ? $member->interests : []);
										}
									}
								}
							}
						}
					}
				}
			}
			
			return view('admin.send_mail')
						->with('menu', 'send_mail')
						->with('sub_menu', 'send_mail')
						->with('interest_group_id', $group_id)
						->with('list_id', $list_id)
						->with('groups', $group_array)
						->with('members', $members_array)
						->with('sms_balance', $this->getSMSBalance());
		}
		else {
			return redirect('/');
		}
  }
 
  //Post import excel
  public function postSendMail(Request $request)
  { 
    $request_data = $request->all();
    $messages = [
      'excel-file.required' => 'Please select excel file name.'
    ];
    
    $validator = Validator::make($request_data, [
      'excel-file' => 'required'
    ], $messages);
    
    if($request['excel-file'] == null || $request['excel-file'] == '')
    {
      return redirect()->back()
      ->withInput()
      ->withErrors([
        'excel-error' 			=> 'Please select a excel file',
      ]);
    }
    
    if($validator->fails())
    { 
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {
			$groups = $all_groups = [];
			if(isset($request_data['group'])){
				$groups = $request_data['group'];
			}
			if(isset($request_data['all-groups'])){
				$all_groups = $request_data['all-groups'];
			}
			$group_json = [];
			foreach($groups as $group) {
				$group_json[] = '"'.$group.'": true';
			}
			foreach($all_groups as $group) {
				if(!in_array($group, $groups)){
					$group_json[] = '"'.$group.'": false';
				}
			}
			
			$interest_json = '';
			if(count($group_json) > 0){
				$interest_json = ',"interests":{'.implode(',', $group_json) .'}';
			}
			
      if(Input::hasFile('excel-file')){
        $path = Input::file('excel-file')->getRealPath();
        $extension = Input::file('excel-file')->getClientOriginalExtension(); 
        if(strtolower($extension) != 'xls' && strtolower($extension) != 'xlsx'){
          //if file is not excel
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please upload a valid file',
          ]);
        }
        $data = Excel::load($path, function($reader) {})->get();
        $totalRows = $data->count();
        if($totalRows > 1500)
        {
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please import maximum 1500 email ids',
          ]);
        }
				$member_details = [];
        foreach($data as $row){
          if(isset($row['email']) && $row['email'] != null && $row['email'] != '') { 
						$user_email = UserEmail::firstOrNew([
							'email' => $row['email']
						]);
						$row['full_name'] = trim(str_replace('.', ' ', str_replace('_', ' ', preg_replace('/[0-9]+/', '', $row['full_name']))));
						$user_email->name = $row['full_name'];
						$user_email->groups = json_encode($groups);
						$user_email->save();
						
						$member_details[] = '{"email_address":"'.$row['email'].'","status":"subscribed","merge_fields":{"FNAME":"'.$row['full_name'].'"}'.$interest_json.'}';         
          }
        }
		//dd($member_details);
				if(count($member_details) <= 0)
				{
					return redirect()->back()
						->withInput()
						->withErrors([
						'excel-error' 			=> 'Please enter correct email ids',
					]);
				}
				
				$curl = curl_init();
				$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
				$group_array = $members_array = [];
				
				curl_setopt_array($curl, array(
					CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_POSTFIELDS => '',
					CURLOPT_HTTPHEADER => array(
						'Authorization: apikey '.$key
					),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				Log::info("==========================Lists=======================");
				Log::info("response");
				Log::info($response);
				Log::info("err");
				Log::info($err);
				Log::info("=================================================");
		
				if ($err) {
					//echo "cURL Error #:" . $err;
				} else {
					if($response !== false) {
						$list_id = 0;
						$data = json_decode($response);
						if(isset($data->lists)){
							foreach($data->lists as $list) {
								$list_id = $list->id;
								break;
							}
							if($list_id != 0) {
				
								curl_setopt_array($curl, array(
									CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/lists/'.$list_id,
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING => '',
									CURLOPT_MAXREDIRS => 10,
									CURLOPT_TIMEOUT => 30,
									CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST => 'POST',
									CURLOPT_POSTFIELDS => '{"members":['.implode(',', $member_details).'], "update_existing": true}',
									CURLOPT_HTTPHEADER => array(
										'Authorization: apikey '.$key
									),
								));

								$response = curl_exec($curl);
								$err = curl_error($curl);

								Log::info("==========================Lists1=======================");
								Log::info("response");
								Log::info($response);
								Log::info("err");
								Log::info($err);
								Log::info("=================================================");
								curl_close($curl);

								if ($err) {
									//echo "cURL Error #:" . $err;
								} else {
									//dd($response);
									//echo $response;
								}
							}
						}
					}
				}
				return redirect('/send-mail')->with('success', 'Emails imported successfully');
				return view('admin.send_mail')
							->with('menu', 'send_mail')
							->with('sms_balance', $this->getSMSBalance());
       
      }
      else{
        return redirect()->back()
        ->withInput()
        ->withErrors([
          'excel-error' 			=> 'Please select a excel file',
        ]);
      }
    }
  }  
  
	public function getEmails() {
		if(auth()->check()) {
			$campaigns = [];
			$curl = curl_init();
			$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
			
			$from_date = '2024-01-01T00:00:05+30:00';
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/campaigns?count=2000&since_send_time='.$from_date,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_POSTFIELDS => '',
				CURLOPT_HTTPHEADER => array(
					'Authorization: apikey '.$key
				),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			Log::info("==========================Campaigns=======================");
			Log::info("response");
			Log::info($response);
			Log::info("err");
			Log::info($err);
			Log::info("=================================================");
			if ($err) {
				echo "cURL Error #:" . $err;
			} else {
				if($response != false) {
					$response = json_decode($response);
					if(isset($response->campaigns)){
						/* foreach($response->campaigns as $campaign) {
							var_dump($campaign);
						} */
						$campaigns = $response->campaigns;
					}
				}
			}
			
			return view('admin.emails')
				->with('campaigns', $campaigns)
				->with('menu', 'send_mail')
				->with('sub_menu', 'emails')
				->with('sms_balance', $this->getSMSBalance());
		}
		else {
			return redirect('/');
		}
	}
	
	//Send mail to user
  public function postFileUpload(Request $request)
  {
    $request_data = $request->all();
 		$image = '';
		if(Input::hasFile('image'))
		{
			$extension = Input::file('image')->getClientOriginalExtension(); 
			if(strtolower($extension) != 'png' && strtolower($extension) != 'jpg' && strtolower($extension) != 'jpeg'){
				return response()->json(array('error' => array('image' => 'Please upload png, jpg or jpeg file')), 400);
			}
			else{
				$image_data = base64_encode(file_get_contents($request->file('image')));
				//$image_data =  "data:image/jpeg;base64," . base64_encode(file_get_contents($request->file('image'))); 
				$extension = $request->file('image')->getClientOriginalExtension();
				$image_name = date('Ymdhis').".".$extension;
				$curl = curl_init();
				$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
				curl_setopt_array($curl, array(
					CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/file-manager/files',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_SSL_VERIFYHOST => false,
					CURLOPT_SSL_VERIFYPEER => false,
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => '{"name":"'.$image_name.'","file_data":"'.$image_data.'"}',
					CURLOPT_HTTPHEADER => array(
						'Authorization: apikey '.$key,
						'content-type: application/json'
					),
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);
				Log::info("==========================Image=======================");
				Log::info("response");
				Log::info($response);
				Log::info("err");
				Log::info($err);
				Log::info("=================================================");
				if ($err) {
				echo "cURL Error #:" . $err;
				} else {
					if($response !== false) {
						$response = json_decode($response);
						if(isset($response->full_size_url)){
							$full_size_url = $response->full_size_url;
							Log::info("full_size_url" .$full_size_url);
							return $full_size_url;
						}
					}
				}
			}			
		} 
		else
		{
			 return response()->json(array('error' => array('image' => 'Please select file')), 400);
		}
	}
  //Send mail to user
  public function postSendMailText(Request $request)
  {
    $request_data = $request->all();
    $template_id = 0;
		$group_id = $request_data['group-id'];
		$list_id = $request_data['list-id'];
		$message = str_replace('"', "'", $request_data['message']);
		$request_data['subject'] = str_replace('"', "'", $request_data['subject']);
		
		Log::info("==========================postSendMailText=======================");
    Log::info("group_id");
    Log::info($group_id);
    Log::info("list_id");
    Log::info($list_id);
		Log::info("message");
    Log::info($message);
		Log::info("=================================================");
		$email_signature = EmailSetting::where('name', 'signature')->pluck('value');
		if($email_signature==null)
		{
			$email_signature = "Best Regards";
		}
    else
		{
			$email_signature = str_replace('"', "'", $email_signature);
		}
    Log::info($email_signature);
		
		$curl = curl_init();
		$key = '525fc684e97c3f03cfb70d6012a67cff-us20';
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/templates',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => '{"name":"Test template","html":"<style> table { display:none; }</style>'.$message.'<br/><br/>'.$email_signature.'"}',
			CURLOPT_HTTPHEADER => array(
				'Authorization: apikey '.$key
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
    Log::info("==========================Template=======================");
    Log::info("response");
    Log::info($response);
    Log::info("err");
    Log::info($err);
		Log::info("=================================================");
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			if($response !== false) {
				$response = json_decode($response);
				if(isset($response->id)){
					$template_id = $response->id;
					//var_dump($template_id);
					Log::info("template_id" .$template_id);
				}
			}
		}
		$campaign_id = 0;
		$conditions = '';
		if(isset($request_data['group']) && (count($request_data['group']) > 0)){
			
			$conditions = [
				'condition_type' => 'Interests', 
				'field'					 => 'interests-'.$group_id, 
				'op'						 => 'interestcontains', 
				'value'					 => $request_data['group']
			];
			$conditions = ',"segment_opts":{"conditions":['.json_encode($conditions).']}';
		}
		
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/campaigns',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => '{"recipients":{"list_id":"'.$list_id.'"'.$conditions.'},"type":"regular","settings":{"subject_line":"'.$request_data['subject'].'","reply_to":"info@prasadlad.com","from_name":"Info Prasad Lad","title":"'.$request_data['subject'].'","template_id":'.$template_id.'}}',
			//info@prasadlad.com
			CURLOPT_HTTPHEADER => array(
				'Authorization: apikey '.$key
			),
		));
	
		$response = curl_exec($curl);
		$err = curl_error($curl);
		Log::info("==========================CAMPAIGN=======================");
    Log::info("json");
    Log::info('{"recipients":{"list_id":"'.$list_id.'"'.$conditions.'},"type":"regular","settings":{"subject_line":"'.$request_data['subject'].'","reply_to":"info@prasadlad.com","from_name":"Info Prasad Lad","title":"'.$request_data['subject'].'","template_id":'.$template_id.'}}');
    Log::info("response");
    Log::info($response);
    Log::info("err");
    Log::info($err);
		Log::info("=================================================");
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			if($response !== false) {
				$response = json_decode($response);
				if(isset($response->id)){
					$campaign_id = $response->id;
				}
			}
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://us20.api.mailchimp.com/3.0/campaigns/'.$campaign_id.'/actions/send',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_SSL_VERIFYHOST => false,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_POST => 1,
				CURLOPT_HTTPHEADER => array(
					'Authorization: apikey '.$key
				),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			Log::info("==========================CAMPAIGN_ID=======================");
			Log::info("response");
			Log::info($response);
			Log::info("err");
			Log::info($err);
			Log::info("=================================================");
			curl_close($curl);
			if ($err) {
				//echo "cURL Error #:" . $err;
			} else {
				//dd($response);
			}
			//TODO update db
		}
		return redirect()->back()->with('success', 'Mail sent successfully!');
  }
	
	public function getEmailSignature() {
		if(auth()->check()){
			
			$signature = EmailSetting::where('name', 'signature')->pluck('value');
			
			return view('admin.mail_signature')
						->with('menu', 'send_mail')
						->with('sub_menu', 'mail_signature')
						->with('signature', $signature)
						->with('sms_balance', $this->getSMSBalance());
		}
		else {
			return redirect('/');
		}
	}
	
	public function postEmailSignature(Request $request) {
		$request_data = $request->all();
		
		$signature = EmailSetting::firstOrNew([
									 'name' => 'signature'
								 ]);
		$signature->value = $request_data['message'];
		$signature->save();
		
		return redirect()->back()->with('success', 'Signature updated successfully!');
	}
	
	public function sendMail($email_ids, $emailnames, $message_text, $title){
		
		Log::info('==================================================');
    foreach($email_ids as $key => $email)
		{
			if($email != "")
			{
				$name = "";
				if(isset($emailnames[$key]))
				{
					$name = $emailnames[$key];
				}
				
				Mail::send('emails.send_mail', array('message_text' => $message_text, 'name' => $name), function($message) use ($email, $name)
				{			
						$message->to($email, $name)->subject('KRSTPL');					
				});
			}	
		}      
    Log::info('Mail Send Successfully: '. date('d-m-Y h:i:s'));
		
	}

	//Send Appointment Sms
	public function getAppointmentSendSMS(){
		return view('admin.send_appointment_sms')->with('menu', 'send_appointment_sms')->with('sub_menu', 'send_appointment_sms')->with('sms_balance', $this->getSMSBalance());
	}

	public function postSendAppointmentSMSText(Request $request)
	{
	    $request_data = $request->all();

	    $messages = [
	        'name.required' => 'Please Enter Name',
	        'name.max' => 'The Name should not exceed 30 characters',
	        'purpose_of_visit.required' => 'Please Enter Purpose of Visit',
	        'purpose_of_visit.max' => 'The Purpose of Visit should not exceed 60 characters',
	        'date.required' => 'Please Select Date',
	        'time.required' => 'Please Select Time',
	        'location.required' => 'Please Enter Location',
	        'location.max' => 'The location should not exceed 60 characters',
	    ];

	    $validator = Validator::make($request_data, [
	        'name' => 'required|max:30',
	        'purpose_of_visit' => 'required|max:60',
	        'date' => 'required',
	        'time' => 'required',
	        'location' => 'required|max:60',
	    ], $messages);

	    if ($validator->fails()) {
	        return redirect()->back()->withErrors($validator)->withInput();
	    } else {
	        if (isset($request_data['name'])) {

	            // $contact_numbers = ['7350878648'];
				$contact_numbers = json_decode($request_data['contacts']);
	            // $message = "Name: {$request_data['name']}\n";
	            // $message .= "Purpose of Visit: {$request_data['purpose_of_visit']}\n";
	            // $message .= "Date: {$request_data['date']}\n";
	            // $message .= "Time: {$request_data['time']}\n";
	            // $message .= "Location: {$request_data['location']}\n";
	            // $message .= "Contacts: {$request_data['contacts']}\n";

	            // $message = nl2br($message);
	            // $message = str_replace('<br />', '\n', $message);

				foreach($contact_numbers as $contact_number){

					$name = $request_data['name'];
					$purpose = $request_data['purpose_of_visit'];
					$location = $request_data['location'];
					$date = $request_data['date'];
					$time = $request_data['time'];

					$dateTime = $date . ' ' . $time;
					$jsonBody = json_encode([
						'template_id' => '62fcbe5bb5812b45ed57b4e2',
						'short_url' => '1', // Change this value as needed
						'mobiles' => '',
						'recipients' => [
							[
								'name' => $name,
								'purpose1' => $purpose,
								'purpose2' => ' at ' . $location,
								'datetime' => $date . ' ' . $time,
								'place1' => $location,
								'place2' => ' India',
							]
						]
					]);

				$url = "https://control.msg91.com/api/v5/flow/";
				$send_data = json_decode($jsonBody, true); 
				$send_data['mobiles'] = '+91'.$contact_number;
				$send_data = json_encode($send_data , true);
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, [
					'accept: application/json',
					'authkey: 206242As8nKOiTzeXx5aba3a76',
					'content-type: application/json',
				]);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					$response = curl_exec($ch);
					if (curl_errno($ch)) {
						echo 'cURL Error: ' . curl_error($ch);
					}
					curl_close($ch);
					Log::info('Response Data: ' .$response);
					Log::info("{\n  \"flow_id\": \"62fcbe5bb5812b45ed57b4e2\",\n  \"sender\": \"PRSLAD\",\n  \"mobiles\":  \"91".$contact_number."\",\n  \"name\": \"$name\",\n  \"purpose1\": \"$purpose\",\n  \"place1\": \"$location\",\n  \"datetime\": \"$dateTime\",\n  \"time\": \"$time,\"\n}");
					
					return $response;
					// if($i==1){
					// 	break;
					// }
					// $send_data = ['template_id'=>'62fcbe5bb5812b45ed57b4e2','short_url'=>'1','recipients'=>['mobiles'=>'919156111159','name'=>'Ameena','purpose1'=>'test','purpose2'=>'','datetime'=> 'current', 'place1'=>'location','place2'=>'']]; 
					
					// $curl = curl_init();
					// curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
					// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					// curl_setopt_array($curl, [
					// 	CURLOPT_URL => "https://control.msg91.com/api/v5/flow/",
					// 	CURLOPT_RETURNTRANSFER => true,
					// 	CURLOPT_ENCODING => "",
					// 	CURLOPT_MAXREDIRS => 10,
					// 	CURLOPT_TIMEOUT => 30,
					// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					// 	CURLOPT_CUSTOMREQUEST => "POST",
					// 	CURLOPT_POSTFIELDS => json_encode($send_data),
					// 	CURLOPT_HTTPHEADER => [
					// 		"accept => application/json",
					// 		"authkey: 206242As8nKOiTzeXx5aba3a76",
					// 		"content-type: application/JSON"
					// 	],
					// ]);
					//old
					//CURLOPT_POSTFIELDS => "{\n  \"template_id\": \"62fcbe5bb5812b45ed57b4e2\",\n  \"sender\": \"PRSLAD\",\n  \"mobiles\":  \"91".$contact_number."\",\n  \"name\": \"$name\",\n  \"purpose1\": \"$purpose\",\n  \"place1\": \"$location\",\n  \"datetime\": \"$dateTime\",\n  \"route\": \4\n}",
					
					//shared by sahar 11 jan
					//{"template_id":"62fcbe5bb5812b45ed57b4e2","short_url":"1 (On) or 0 (Off)","recipients":[{"mobiles":"918888280665","name":"Sahar","purpose1":"Meeting","purpose2":"at Bandra","datetime":"23 Jan 2024 12 PM","place1":"Bandra","place2":"Mumbai"}]}

					//'{"template_id":"62fcbe5bb5812b45ed57b4e2","short_url":"1 (On) or 0 (Off)","recipients":[{"mobiles":"917350878648","name":"Sahar","purpose1":"Meeting"}]}',
					
					  //$postfield="{\n  \"flow_id\": \"62f780a64c6773597966b539\",\n  \"sender\": \"PRSLAD\",\n  \"mobiles\": \"91".$contact_number."\",\n  \"date\": \"$appointment_date\",\n  \"time\": \"$appointment_time\"\n}";
					// $response = curl_exec($curl);
					// Log::info('Response:');
					// Log::info($response);
					// Log::info("{\n  \"flow_id\": \"62fcbe5bb5812b45ed57b4e2\",\n  \"sender\": \"PRSLAD\",\n  \"mobiles\":  \"91".$contact_number."\",\n  \"name\": \"$name\",\n  \"purpose1\": \"$purpose\",\n  \"place1\": \"$location\",\n  \"datetime\": \"$dateTime\",\n  \"time\": \"$time,\"\n}");
				   	// $err = curl_error($curl);
					//    dd($err);
					// Log::info('Error: ');
					// Log::info($err);
					// curl_close($curl);

					// if ($err) {
					//  	echo "cURL Error #:" . $err;
					// } else {
					// 	// echo $response;
					// 	return back()->with('success', 'Messages Sent Successfully');
					// }

					// Log::info('Message sent to: ' .$contact_number. '\n');
				}

	            return redirect()->back()->with('success', 'Messages Sent Successfully');
	        }
	    }
	}
 	
	//Send message using template id
	//Date: 14122023
	//Komal Bhosale
	public function sendTemplateMessage($contact_numbers, $send_data){	
		for($i=0; $i<count($contact_numbers); $i++)
		{  
			$contact_number = trim($contact_numbers[$i]);
			Log::info('Send message to: ' .$contact_number);

			$url = "https://control.msg91.com/api/v5/flow/";
			$send_data = json_decode($send_data, true); 
			$send_data['mobiles'] = '+91'.$contact_number;
			$send_data = json_encode($send_data , true);
			// dd($send_data,$contact_number);
			Log::info('Data: ' .$send_data);
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'accept: application/json',
				'authkey: 206242As8nKOiTzeXx5aba3a76',
				'content-type: application/json',
			]);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($ch);
			if (curl_errno($ch)) {
				Log::error('cURL Error: ' . curl_error($ch));
			}
			curl_close($ch);
			Log::info('Response Data: ' . $response);
			echo $response;
			// if($i==1){
			// 	break;
			// }
			// $curl = curl_init();
			// curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt_array($curl, [
			// 	CURLOPT_URL => $url,
			// 	CURLOPT_RETURNTRANSFER => true,
			// 	CURLOPT_ENCODING => "",
			// 	CURLOPT_MAXREDIRS => 10,
			// 	CURLOPT_TIMEOUT => 30,
			// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			// 	CURLOPT_CUSTOMREQUEST => "POST",
			// 	CURLOPT_POSTFIELDS => $send_data,
			// 	CURLOPT_HTTPHEADER => [
			// 		"authkey: 206242As8nKOiTzeXx5aba3a76",
			// 		"content-type: application/JSON"
			// 	],
			// ]);
			// $response = curl_exec($curl);
			// curl_close( $curl );
			// Log::info('Response Data: ' .$response);
			// return $response;
		}
		// return 0;			
	}
}
