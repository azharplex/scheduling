<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Session;
use Validator;
use DB;

use App\Channel;

class ChannelController extends Controller
{
  //View all channel
  public function getIndex() 
	{
		if(Auth::check() && Auth::User()->user_type == 0)
    {
      $channels = Channel::orderBy('created_at')->get();
			return view('admin.view_channel')
			->with('channels', $channels)
			->with('menu', 'channel')
      ->with('sub_menu', 'view_channel');
    }
    else
    {
      return redirect()->to('/');
    }
	}
	
  //get add channel
  public function getAdd()
  {
    if(Auth::check())
    {
      Session::forget('menu');
      return view('admin.add_channel')
      ->with('menu', 'channel')
      ->with('sub_menu', 'add_channel');
    }
    else
    {
      return redirect()->to('/');
    }
  } 
  
  //Add Channel 
  public function postAdd(Request $request)
  {
    $request_data = $request->all();
    
    $messages = [
      'channel.required' => 'Please enter channel name.',
    ];
    
    $validator = Validator::make($request_data, [
			'channel' => 'required|min:2|max:50|unique:channel',
			'description' => 'min:2|max:850'
		], $messages);

    if($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {
      $obj_channel = new Channel;
      $obj_channel->channel = $request_data['channel'];
      $obj_channel->description = $request_data['description'];
      $obj_channel->contact_person = $request_data['contact-person'];
      $obj_channel->contact_person_designation = $request_data['contact-person-designation'];
      $obj_channel->contact_person_numbers = $request_data['contact-person-number'];
      $obj_channel->status = 1;
      $obj_channel->save();      
      return redirect()->back()->with('success', 'Channel added successfully');
    }
  }
  
  //Get edit channel 
  public function getEdit(Request $request)
  {
    if(Auth::check())
    {
      Session::forget('menu');
      $request_data = $request->all();
      $channel = Channel::where('id', $request_data['channel-id'])->first();
      return view('admin.edit_channel')
      ->with('channel', $channel)
      ->with('menu', 'channel');
    }
    else
    {
      return redirect()->to('/');
    }
  } 
  
  //Edit channel 
  public function postEdit(Request $request)
  {
    $request_data = $request->all();
    $messages = [
      'channel.required' => 'Please enter channel name.',
    ];
    
    $validator = Validator::make($request_data, [
			'channel' => 'required|min:2|max:50',
			'description' => 'min:2|max:850'
		], $messages);
    
    if($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {  
      $obj_channel = Channel::find($request_data['channel-id']);
      $old_channel= $obj_channel->channel;
      $new_channel = $request_data['channel'];
      if($old_channel != $new_channel)
      {
        $channel = Channel::where('channel', $new_channel)->get();
        if(count($channel) > 0)
        {
          $error =  array('channel' => 'Channel name already exist');
          return redirect()->back()->withErrors($error)->withInput();
        }
        else
        {
          $obj_channel->channel = $request_data['channel'];
        }        
      }  
      $obj_channel->description = $request_data['description'];
      $obj_channel->contact_person = $request_data['contact-person'];
      $obj_channel->contact_person_designation = $request_data['contact-person-designation'];
      $obj_channel->contact_person_numbers = $request_data['contact-person-number'];
      $obj_channel->status = $request_data['status'];
      $obj_channel->save();  
    } 
    
    return redirect()->back()->with('success', 'Channel updated successfully');    
  }  
	
  //Delete channel
  public function postDelete(Request $request)
  {
    $request_data = $request->all();
    $channel_id = $request_data['channel-id']; 
    $channel = DB::table('channel')->where('id', $channel_id)->delete();
    return redirect()->to('channel')->with('success', 'Channel deleted successfully');
  }
}
