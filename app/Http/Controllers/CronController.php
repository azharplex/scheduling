<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Wish;
use App\Appointment;
use App\Message;

use Mail;
use Auth;
use DB;
use Log;

class CronController extends Controller {  
  public function __construct(HomeController $home_controller)
  {
    $this->home_controller = $home_controller;
  }

  //Send Today Wishes [php artisan sendtodaywishes]
  public function SendTodayWishes()
  {
    $current_date = date('Y-m-d');
    
    $birthdays = Wish::whereDay('birth_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($current_date)))->get();
    
    $anniversaries = Wish::whereDay('anniversary_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($current_date)))->get();
    
    for($i=0; $i<count($birthdays); $i++)
    {
      $birthday_list[] = $birthdays[$i]->full_name;
    }
    
    for($j=0; $j<count($anniversaries); $j++)
    {
      $anniversary_list[] = $anniversaries[$j]->full_name; 
    }   
    $title = 'Todays Wish List';  
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];  
    if(count($birthdays) > 0)
    {
      $recipients = [];
      foreach ($birthday_list as $key => $name) {
        $recipients["name" . ($key + 1)] = $name;
        }
    $jsonBody = json_encode([
      'template_id' => '6561f6a99cfd5344436b1b02',
      'short_url' => '1', 
      'mobiles' => '',
      'recipients' => [$recipients],
    ]);
    // dd($jsonBody);
    $message= $jsonBody;
    Log::info('Date  : ' .$current_date);  
    Log::info('Birthday Wish List : ' .$message);  
    
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    }
    if(count($anniversaries) > 0)
    {
      $recipients=[];
    foreach ($anniversary_list as $key => $name) {
      $recipients["name" . ($key + 1)] = $name;
    }
    $jsonBody = json_encode([
      'template_id' => '6561f7559cfd5344436b1b03',
      'short_url' => '1', 
      'mobiles' => '',
      'recipients' => [$recipients],
    ]);
    $message = $jsonBody;
    Log::info('Date  : ' .$current_date);  
    Log::info('Anniversary Wish List : ' .$message);  
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title); 
    }
    // $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');

    /*Mail::send('emails.todays_wishes', array('birthdays' => $birthdays, 'anniversaries' => $anniversaries), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Todays Wish List');
        }
      }
    });*/
            
    Log::info('Todays Wish List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Tomorrow Wishes [php artisan sendtomorrowwishes]
  public function SendTomorrowWishes()
  {
    $current_date = date('Y-m-d');
    $tomorrow_date = date("Y-m-d",strtotime("+1days",strtotime($current_date)));

    $birthdays = Wish::whereDay('birth_date', '=', date('d', strtotime($tomorrow_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($tomorrow_date)))->get();
    
    $anniversaries = Wish::whereDay('anniversary_date', '=', date('d', strtotime($tomorrow_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($tomorrow_date)))->get();
    
    $birthday_list = "";
    $anniversary_list = "";
    
    for($i=0; $i<count($birthdays); $i++)
    {
      $birthday_list .= $birthdays[$i]->full_name . '\n'; 
    }
    
    for($j=0; $j<count($anniversaries); $j++)
    {
      $anniversary_list .= $anniversaries[$j]->full_name . '\n'; 
    }
    
    Log::info('Date  : ' .$current_date);  
    Log::info('Wish List : ' .$birthday_list);  
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];  
    $message = 'Tomorrows Wish List\n\n';  
    if(count($birthdays) > 0)
    {
      $message .= 'Birthdays :  \n' .$birthday_list;  
    }
    else
    {
      $message .= 'No Birthdays For Tomorrow \n';  
    }
    $message .= '~~~~~~~~~~~~~ \n'; 
    if(count($anniversaries) > 0)
    {
      $message .= 'Anniversaries : \n' .$anniversary_list; 
    }
    else
    {
      $message .= 'No Anniversaries For Tomorrow \n';  
    }
    $title = 'Tomorrows Wish List';  
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');
    Mail::send('emails.tomorrows_wishes', array('birthdays' => $birthdays, 'anniversaries' => $anniversaries), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Tomorrows Wish List');
        }
      }
    });
    Log::info('Tomorrows Wish List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Todays Appointment [php artisan sendtodayappointmentlist]
  public function SendTodayAppointmentList()
  {
    $current_date = date('Y-m-d');
    $tomorrow_date = date("Y-m-d",strtotime("+1days",strtotime($current_date)));
    $appointments = Appointment::whereDate('appointment_date', '=', $current_date)->OrderBy(DB::raw("STR_TO_DATE(appointment_time, '%h:%i %p')"), 'ASC')->get();
    $appointment_count= count($appointments) > 0 ;
    $admins = $this->getAdminList();
      $contact_numbers = $admins['numbers'];  
      $name = '';
      $purpose = '';
      $datetime = '';
      $location1 = '';
      $location2 = '';
   if($appointment_count == true){
    foreach($appointments as $appointment)
      {
      $name=$appointment->full_name;
      $purpose1=substr($appointment->purpose_of_visit,0,30);
      $purpose2=substr($appointment->purpose_of_visit,30,30);
      $purpose=$purpose1.$purpose2;
      $date=$appointment->appointment_date;
      $time=$appointment->appointment_time;
      $datetime=$date."".$time;
      $location1=substr($appointment->location,0,30);
      $location2=substr($appointment->location,30,30);
      $jsonBody = json_encode([
        'template_id' => '62fcbe5bb5812b45ed57b4e2',
        'short_url' => '1', 
        'mobiles' => '',
        'recipients' => [
          [
            'name' => $name,
            'purpose1' => $purpose,
            'purpose2' => ' at ' . $location1,
            'datetime' => $date . ' ' . $time,
            'place1' => $location1,
            'place2' => $location2,
          ]
        ]
      ]);
    }
  }
  if($appointment_count == false){
    $jsonBody = json_encode([
      'template_id' => '6561f7c34d53ac76466c8b4d',
      'short_url' => '1', 
      'mobiles' => '',
    ]);
  }
  // dd($jsonBody);
  $send_data= $jsonBody;
  $title= 'Todays Appointment List';
  $response_data = $this->home_controller->sendTemplateMessage($contact_numbers, $send_data);   
  Log::info('Date  : ' .$current_date);
  Log::info('Appointment List : ' .'\n'.'Name:'.$name.'\n'.$purpose.'\n'.$datetime.'\n'.$location1.'\n'.$location2.'\n'); 
    Log::info('==================================================');
    Log::info('Todays Appointment List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Tomorrow Appointment [php artisan sendtomorrowappointmentlist]
  public function SendTomorrowAppointmentList()
  {
    $current_date = date('Y-m-d');
    $tomorrow_date = date("Y-m-d",strtotime("+1days",strtotime($current_date)));

    $appointments = Appointment::whereDate('appointment_date', '=', $tomorrow_date)->OrderBy(DB::raw("STR_TO_DATE(appointment_time, '%h:%i %p')"), 'ASC')->get();  

    $appointment_list = "";
    
    for($i=0; $i<count($appointments); $i++)
    {
      $jsonBody = json_encode([
        'template_id' => '6561f8a9b807044e3a395ced',
        'short_url' => '1', 
        'mobiles' => '',
        'recipients' => [
          [
            'name' => $appointments[$i]->full_name,
            'purpose1' => $appointments[$i]->purpose_of_visit,
            'datetime' => date('d-F h:i a', strtotime($appointments[$i]->appointment_date .' '. $appointments[$i]->appointment_time)),
            'place1' => $appointments[$i]->location,
          ]
        ]
      ]);
    }
    
    Log::info('Date  : ' .$current_date);  
    Log::info('Appointment List : ' .$appointment_list);  
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];  
    if(count($appointments) > 0)
    {
      $message = $jsonBody;  
    }
    else
    {
      $message = 'No Appointments For Tomorrow \n';  
    }
 
    $title = 'Tomorrows Appointment List';  
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');
    Mail::send('emails.tomorrows_appointment', array('appointments' => $appointments), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Tomorrows Appointment List');
        }
      }
    });
    Log::info('Tomorrows Appointment List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Wishes to user [php artisan sendwishes]
  public function SendWishes()
  {
    $current_date = date('Y-m-d');

    $birthdays = Wish::whereDay('birth_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($current_date)))->get();
    
    $anniversaries = Wish::whereDay('anniversary_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($current_date)))->get();
    
    $birthday_message = 'Happy Birthday to you';    
    $anniversary_message = 'Happy Anniversary to you and your wife'; 
      
    $message_data = Message::first(); 
    
    if(count($message_data) > 0)
    {
      $birthday_message = $message_data->birthday_message;    
      $anniversary_message = $message_data->anniversary_message; 
    }          
   
    $b_message = $birthday_message .' \n\nFrom \nMr. Prasad Lad \nMLC & Vice President BJP- Maharastra \n'; 
    
    for($i=0; $i<count($birthdays); $i++)
    {
      $name = $birthdays[$i]->full_name;
      $email = $birthdays[$i]->email;
      $contact_numbers = explode(' / ', $birthdays[$i]->contact_number);
      $title = 'Happy Birthday';  
      $response_data = $this->home_controller->sendMessage($contact_numbers, $b_message, $title); 
      if($email != "")
      {
        Mail::send('emails.birthday_wishes', array('email' => $email, 'name' => $name, 'b_message' => $birthday_message), function($message) use ($email, $name)
        {
          $message->to($email, $name)->subject('Happy Birthday');
        });
      }
      Log::info('Date  : ' .$current_date);  
      Log::info('Birthday email : ' .$email); 
    }
    
    $a_message = $anniversary_message .'\n\nFrom \nMr. Prasad Lad \n'; 
    for($j=0; $j<count($anniversaries); $j++)
    {
      $name = $anniversaries[$j]->full_name;
      $email = $anniversaries[$j]->email;
      $contact_numbers = [$anniversaries[$j]->contact_number];
      $title = 'Happy Anniversary';  
      $response_data = $this->home_controller->sendMessage($contact_numbers, $a_message, $title); 
      if($email != "")
      {
        Mail::send('emails.anniversary_wishes', array('email' => $email, 'name' => $name, 'a_message' => $anniversary_message), function($message) use ($email, $name)
        {
          $message->to($email, $name)->subject('Happy Anniversary');
        });
      }
      Log::info('Date  : ' .$current_date);  
      Log::info('Anniversary email : ' .$email); 
    }       
    Log::info('==================================================');
    Log::info('Wishes Message & Email Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Get admin list to send mails and sms
  public function getAdminList()
  {
    $users = User::where('id', '!=', 1)->get();
    $numbers = User::where('id', '!=', 1)->lists('number')->toArray();
    return array('numbers' => $numbers, 'users' => $users);
  }
}
