<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use Log;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Validator;

class AppointmentController extends Controller
{
    public function __construct(HomeController $home_controller)
    {
      $this->home_controller = $home_controller;
      $this->sms_balance = $this->home_controller->getSMSBalance();
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function appointmentAdd()
    {
       return view('admin.add_appointment')->with('menu','appointments')->with('sub_menu', 'appointment/add')->with('sms_balance', $this->sms_balance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function appointmentsave(Request $request)
    {


        try{
            $request_data = $request->all();
//            $email = $request_data['user_email'];

            $messages = [

                'appointment_date.required' => 'Please enter appointment date',
                'appointment_time.required' => 'Please enter appointment time',
                'name.required' => 'Please enter full name',
    	        'name.max' => 'The Name should not exceed 30 characters',
                'type.required' => 'Please enter contact number',
                'contact_number.required' => 'Please enter contact number',
                'contact_number.numeric' => 'Contact number must be numeric',
                'contact_number.regex' => 'Contact number must be 10 digits',
                'bdate.required' => 'Please enter birth date',
                'any_date.required' => 'Please enter anniversary date',
                'designition.required' => 'Please enter designation',
                'email.required' => 'Please enter email',
                'purpose_of_visit.required' => 'Please enter purpose of visit',
                'purpose_of_visit.max' => 'The Purpose of Visit should not exceed 60 characters',
                'description.required' => 'Please enter work description',
                'location.required' => 'Please enter location details',
                'location.max' => 'The location should not exceed 60 characters',
            ];

//            dd($request);
            $validator = Validator::make($request_data, [
                'appointment_date' => 'required|date',
                'appointment_time' => 'required',
                'name' => 'required|max:30',
                'contact_number' => 'numeric|required|regex:/^[0-9]{10}$/',
                'purpose_of_visit' => 'required |max:60',
                'description' => 'required',
                'email' => 'email|max:255',
                'important' => 'required',
                'location' => 'required|max:60',
            ], $messages);

            if($validator->fails())
            {
//                return redirect()->back()->withErrors($validator, 'login');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if(!isset($request->appointment_id)){
                $apointment = new Appointment();
            }else{
                $apointment = Appointment::find($request->appointment_id);
            }

            $apointment->appointment_date	 = date('Y-m-d', strtotime($request->appointment_date));
            $apointment->appointment_time	 = $request->appointment_time;
            $apointment->full_name	 = $this->str_correction($request->name);
            $apointment->contact_number = $request->contact_number;
            $apointment->email	 = $request->email;
            $apointment->purpose_of_visit	 = $this->str_correction($request->purpose_of_visit);
            $apointment->description	 = $this->str_correction($request->description);
            $apointment->location	 = $this->str_correction($request->location);
            $apointment->important	 = $request->important;
            $full_name = $apointment->full_name;
            $user_email = $apointment->email;
            $appointment_date = $apointment->appointment_date;
            $appointment_time = $apointment->appointment_time;	
            $apointment->save();
            
            if(isset($apointment->id)){
              Log::info('Message Appointment ID : ' .$apointment->id);  
              Log::info('Message Appointment Time : ' .$apointment->appointment_time);  
              $contact_numbers = [$apointment->contact_number];  
              $message = 'Thank you \n \n Your Appointment is on :\n' . date('d F Y', strtotime($request->appointment_date)) . ' at ' .date('h:i A', strtotime($request->appointment_time));  
              $title = 'Appointment Rescheduled'; 
                $date = $request_data['appointment_date'];
                $time = $request_data['appointment_time'];
                $jsonBody = json_encode([
                    'template_id' => '62f780a64c6773597966b539',
                    'short_url' => '1',
                    'mobiles'   => '',
                    'recipients' => [
                        [
                            'name' => $full_name,
                            'date' => $date,
                            'time' => $time
                        ]
                    ]
                ]);

                // dd($apointment,$jsonBody);
                $response_data = $this->home_controller->sendTemplateMessage($contact_numbers, $jsonBody); 
            Log::info('==================================================');              
              if($user_email != "")
              {
                Mail::send('emails.update_appointment', array('full_name' => $full_name, 'appointment_date' => $appointment_date, 'appointment_time' => $appointment_time), function($message) use ($user_email, $full_name)
                {
                  $message->to($user_email, $full_name)->subject('Appointment rescheduled with Mr. Prasad Lad');
                });
              }
              return redirect()->back()->with('success','Appointment updated successfully');
            }
            
            Log::info('Message Appointment ID : ' .$apointment->id);  
            Log::info('Message Appointment Time : ' .$apointment->appointment_time);  
            $contact_numbers = [$apointment->contact_number];  
            $message = 'Thank you \n \n Your Appointment is on :\n' . date('d F Y', strtotime($request->appointment_date)) . ' at ' .date('h:i A', strtotime($request->appointment_time));  
            $title = 'Appointment Confirmation'; 
            $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
            Log::info('==================================================');      
            
            $full_name = $apointment->full_name;
            $user_email = $apointment->email;
            $appointment_date = $apointment->appointment_date;
            $appointment_time = $apointment->appointment_time;
            if($user_email != "")
            {
              Mail::send('emails.add_appointment', array('full_name' => $full_name, 'appointment_date' => $appointment_date, 'appointment_time' => $appointment_time), function($message) use ($user_email, $full_name)
              {
                $message->to($user_email, $full_name)->subject('Appointment confirmation with Mr. Prasad Lad');
              });
            }

            return redirect()->back()->with('success','Appointment added successfully');
        }catch (Exception $exception){
            return redirect()->back()->with('error',$exception);
        }
    }
		
		public function str_correction($string){
			return str_replace(".  ", ". ", str_replace(".", ". ", $string));
    }
		
    public function appointmentView(Request $request){
        $appointment = Appointment::find($request->id);
        return view('admin.appointment_view')->with('menu','appointments')->with('sub_menu', 'appointments')->with('appointment',$appointment)->with('sms_balance', $this->sms_balance);
    }

    public function appointments(Request $request){

 
        $type = "";
        $start_date = date('d-m-Y');
        $end_date = date("d-m-Y",strtotime("+30days",strtotime($start_date)));
        $appointments = Appointment::orderBy('created_at')->where('appointment_date','>=',date('Y-m-d',strtotime($start_date)))->where('appointment_date','<=',date('Y-m-d',strtotime($end_date)))->get();
        if(isset($request->type) || isset($request->filter_date)){
            if(isset($request->type)){
                $type = $request->type;
            }


            $start_date = explode('-',$request->filter_date)[0];
            $end_date = explode('-',$request->filter_date)[1];
            
            $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
            $end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));
            
            $appointments = Appointment::select('*');
            if (isset($request->type) && !empty($request->type)){
                $appointments = $appointments ->where('important',$request->type);
            }
            if(isset($request->filter_date)){
                $date_range = $request->filter_date;
                $appointments = $appointments->where('appointment_date','>=',date('Y-m-d',strtotime($start_date))) ->where('appointment_date','<=',date('Y-m-d',strtotime($end_date)));

            }
            $appointments = $appointments->get();

        }
//        dd($request);
        return view('admin.appointments_history')->with('appointments',$appointments)->with('menu','appointments')->with('sub_menu', 'appointments')->with('end_date',date("d/m/Y", strtotime($end_date)))->with('start_date',date("d/m/Y", strtotime($start_date)))->with('sms_balance', $this->sms_balance);

    }

    public function appointmentEdit(Request $request){
        $app = Appointment::find($request->id);
        return view('admin.add_appointment')->with('menu','appointments')->with('sub_menu', 'appointments')->with('appointment',$app)->with('sms_balance', $this->sms_balance);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function appoinmentDelete(Request $request)
    {
        Appointment::find($request->id)->delete();
        return redirect()->back()->with('success','Appointment deleted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
