<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;
use Log;
use DB;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class UserTrackController extends Controller
{
   public function __construct(HomeController $home_controller)
    {
      $this->home_controller = $home_controller;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function trackingDetails(Request $request)
    {
         if(isset($request->track_id)){

             $request_data = $request->all();

             $messages = [
                 'track_id.required' => 'Please enter Track ID',
                 'track_id.max' => 'Track ID mustbe 6 digit',
                 'track_id.min' => 'Track ID mustbe 6 digit',
             ];

             $validator = Validator::make($request_data, [
                 'track_id' => 'required|max:6|min:6',

             ], $messages);

             if($validator->fails())
             {
                 return redirect()->back()->withErrors($validator)->withInput();
             }
             $visitor = Visitor::where('tracking_id',$request->track_id)->first();

             if(!empty($visitor)){   
                $otp = rand(1111, 9999);
                Log::info('Message Visitor ID : ' .$visitor->id);  
                Log::info('OTP : ' .$otp);  
                $email_otp = $visitor->email;  
                $name = $visitor->full_number;  
                $contact_numbers = [$visitor->contact_number];  
                $message = 'Your OTP : ' .$otp;  
                $title = 'OTP';  
                $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title); 
                DB::table('visitors')
                ->where('tracking_id', $request->track_id)
                ->update(['otp_id' => $otp]);    
                if($email_otp != "")
                {                  
                  Mail::send('emails.otp', array('otp' => $otp, 'name' => $name), function($message) use ($email_otp, $name)
                  {
                    $message->to($email_otp, $name)->subject('Your OTP');
                  });   
                }                
                Log::info('==================================================');
                 return redirect()->to('/tracking/otp/'.$request->track_id);             
                 //return view('user.track_details')->with('visitor',$visitor);

             }else{
                 return back()->with('error','Track ID is Invalid');
             }

         }else{
             return redirect('/track/schedule');
         }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkOTP(Request $request)
    {
         if(isset($request->otp)){

             $request_data = $request->all();

             $messages = [
                 'track_id.required' => 'Please enter Track ID',
                 'otp.required' => 'Please enter OTP'
             ];

             $validator = Validator::make($request_data, [
                 'track_id' => 'required|max:6|min:6',
                 'otp' => 'required',

             ], $messages);

             if($validator->fails())
             { 
                 return redirect()->back()->withErrors($validator)->withInput();
             }
             $visitor = Visitor::where('tracking_id',$request->track_id)->where('otp_id', $request->otp)->first();

             if(!empty($visitor)){                 
                 return view('user.track_details')->with('visitor',$visitor);

             }else{
              
                 return redirect()->back()->with('error','OTP is Invalid');
             }

         }else{
            if(!isset($request->track_id))
            {
             return redirect('/track/schedule');
            }
            else{
              return view('user.otp')->with('track_id', $request->track_id);
            }
         }
    }

    public function getOTP($track_id)
    {
        if(!isset($track_id))
        {
         return redirect('/track/schedule');
        }
        else{
          return view('user.otp')->with('track_id', $track_id);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
