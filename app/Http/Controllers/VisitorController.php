<?php

namespace App\Http\Controllers;

use App\trackingComent;
use App\Visitor;
use Illuminate\Http\Request;
use Log;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class VisitorController extends Controller
{
    public function __construct(HomeController $home_controller)
    {
      $this->home_controller = $home_controller;
      $this->sms_balance = $this->home_controller->getSMSBalance();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function visitorsAdd()
    {
        return view('admin.add_visitor')->with('menu', 'visitors')->with('sub_menu', 'add_visitors')->with('sms_balance', $this->sms_balance);
    }
    public function visitorsave(Request $request){
        try{
            $request_data = $request->all();
            $messages = [

                'name.required' => 'Please enter full name',
                'name.max' => 'The Name should not exceed 30 characters',
                'date_of_visit.required' => 'Please enter date of visit',
                'type.required' => 'Please enter contact number',
                'contact_number.required' => 'Please enter contact number',
                'contact_number.numeric' => 'Contact number must be numeric',
                'contact_number.regex' => 'Contact number must be 10 digits',
                'bdate.required' => 'Please enter birth date',
                'any_date.required' => 'Please enter anniversary date',
                'designition.required' => 'Please enter designation',
                'email.required' => 'Please enter email',
                'purpose_date.required' => 'Please enter purpose of visit',
                'description.required' => 'Please enter work description',
            ];
            $validator = Validator::make($request_data, [
                'date_of_visit' => 'required',
                'name' => 'required|max30',
                'type' => 'required',
                'contact_number' => 'numeric|required|regex:/^[0-9]{10}$/',
                //'designition' => 'required',
                'purpose_date' => 'required',
                'description' => 'required',
                'email' => 'email|max:255',
            ], $messages);

            if($validator->fails())
            {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $is_add = false;
            if(!isset($request->visitor_id)){
                $visitor = new Visitor();
                $is_add = true;
            }else{
                $visitor = Visitor::find($request->visitor_id);
            }

            $visitor->full_name	 = $request->name;
            $visitor->type	 = $request->type;
            $visitor->date_of_visit	 = date('Y-m-d', strtotime($request->date_of_visit)); 
            if($request->type == 'official'){              
                if(!isset($request->visitor_id)){
                  $visitor->tracking_id	 = rand(100000,999999);
                }        
            }            
            $visitor->designition	 = $request->designition;
            $visitor->contact_number = $request->contact_number;
            $visitor->email	 = $request->email;
            $visitor->department	 = $request->department;
            $visitor->purpose_of_visit	 = $request->purpose_date;
            $visitor->work_description	 = $request->description;
            $visitor->save();
            $contact_numbers = [$visitor->contact_number]; 
            $full_name = $visitor->full_name;
            $user_email = $visitor->email;
            $tracking_id = $visitor->tracking_id;
            $send_data = '';
            if($is_add && $request->type == 'official'){
                $jsonBody = json_encode([
                    'template_id' => '656ede4b57ca5259ca5793c3',
                    'short_url' => '1',
                    'sender' => 'PRSLAD',
                    'mobiles' => '',
                    'recipients' => [
                        [
                            'name' => $visitor->full_name,
                            'trackno' => $visitor->tracking_id,
                        ]
                    ]
                ]);
                $send_data = $jsonBody;
                // dd($send_data);
                $response_data = $this->home_controller->sendTemplateMessage($contact_numbers, $send_data);
            }           
                
            if(!isset($request->visitor_id)){
                $visitorstatus = Visitor::find($visitor->id);
                if($request->type == 'general'){
                    $visitorstatus->status = 'Closed';
                }
                $visitorstatus->save();

                Log::info('Message Visitor ID : ' .$visitor->id);  
                Log::info('Message Tracking ID : ' .$visitor->tracking_id);      

                if($request->type == 'official'){
                    $message = 'Thank you for Visiting.\n \n Your Tracking ID : ' .$visitor->tracking_id;  
                    $track_url = Config('app.url') .'track/schedule'; 
                    $message .= '\n \n Kindly use below link to track your ticket \n '. $track_url;  
                    $title = 'Tracking ID';                      
                }
                else{
                    $message = 'Thank you for Visiting Mr. Prasad Lad - MLC & Vice President BJP- Maharastra.';  
                    $title = 'Thank you for Visiting'; 
                }
                Log::info('==================================================');                    
                if($user_email != "")
                {
                    Mail::send('emails.add_visitor', array('full_name' => $full_name, 'tracking_id' => $tracking_id), function($message) use ($user_email, $full_name)
                    {
                        $message->to($user_email, $full_name)->subject('Thank you for visiting Mr. Prasad Lad');
                    });
                }
            }
            if(isset($request->visitor_id)){
                if($request->type == 'official'){
                    return redirect()->back()->with('success','Visitor Update successfully')->with('type','official')->with('name',$request->name)->with('track_id',$visitor->tracking_id);
                }            
            }
            if($request->type == 'official'){
                return redirect()->back()->with('success','Visitor Update successfully')->with('type','official')->with('name',$request->name)->with('track_id',$visitor->tracking_id);
            }
            return redirect()->back()->with('success','Visitor added successfully');
        }catch (Exception $exception){
            return redirect()->back()->with('error',$exception);
        }
    }

    public function Visitors(Request $request){

        $type = "";
        $current_date = date('d-m-Y');
        $start_date = date("d-m-Y",strtotime("-30days",strtotime($current_date)));
        $end_date = date('d-m-Y');
       
        
        if(isset($request->type) || isset($request->filter_date)){
              if(isset($request->type)){
                  $type = $request->type;
              }


            $start_date = explode('-',$request->filter_date)[0];
            $end_date = explode('-',$request->filter_date)[1];
            
            $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
            $end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));
      
            $visitors = Visitor::select('*');
           if (isset($request->type) && !empty($request->type)){
               $visitors = $visitors ->where('type',$request->type);
           }
           if(isset($request->filter_date)){
               $date_range = $request->filter_date;
      
               $visitors = $visitors->where('date_of_visit','>=',date('Y-m-d',strtotime($start_date))) ->where('date_of_visit','<=',date('Y-m-d',strtotime($end_date)));         
           }
           $visitors = $visitors->orderBy('date_of_visit', 'desc')->get();
        }else{

            $visitors = Visitor::where('date_of_visit','>=',date('Y-m-d',strtotime($start_date))) ->where('date_of_visit','<=',date('Y-m-d',strtotime($end_date)))->orderBy('date_of_visit', 'desc')->get();  
            //$visitors = Visitor::all();
        }

        return view('admin.visitors')->with('visitors',$visitors)->with('menu', 'visitors')->with('start_date',date("d/m/Y", strtotime($start_date)))->with('end_date',date("d/m/Y", strtotime($end_date)))->with('sub_menu', 'view_visitors')->with('type',$type)->with('sms_balance', $this->sms_balance);
    }

    public function visitorView(Request $request){
        $visitor = Visitor::find($request->id);
        return view('admin.view_visitor')->with('menu', 'visitors')->with('sub_menu', 'view_visitors')->with('visitor',$visitor)->with('sms_balance', $this->sms_balance);
    }
   public function Update(Request $request){
        $visitor = Visitor::find($request->id);
        return view('admin.add_visitor')->with('menu', 'visitors')->with('sub_menu', 'view_visitors')->with('visitor',$visitor)->with('sms_balance', $this->sms_balance);
   }
   public function trackingDetails(Request $request){

      $type = "";
      $current_date = date('d-m-Y');
      $start_date = date("d-m-Y",strtotime("-1year",strtotime($current_date)));
      $end_date = date("d-m-Y",strtotime($current_date));


       if(isset($request->type) || isset($request->filter_date)){
           if(isset($request->type)){
               $type = $request->type;
           }
           
           $start_date = explode('-',$request->filter_date)[0];
           $end_date = explode('-',$request->filter_date)[1];
          $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
          $end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));
            
           $visitors = Visitor::select('*');
           if (isset($request->type) && !empty($request->type)){
               $visitors = $visitors ->where('status',$request->type);
           }
           if(isset($request->filter_date)){
               $date_range = $request->filter_date;
               $visitors = $visitors->where('type', '=', 'official')->where('date_of_visit','>=',date('Y-m-d',strtotime($start_date))) ->where('date_of_visit','<=',date('Y-m-d',strtotime($end_date)));

           }
           $visitors = $visitors->get();

       }else{

           $visitors = Visitor::where('status', '=', 'In progress')->where('type', '=', 'official')->where('date_of_visit','>=',date('Y-m-d',strtotime($start_date))) ->where('date_of_visit','<=',date('Y-m-d',strtotime($end_date)))->get();
       }

       return view('admin.tracking_details')->with('visitors',$visitors)->with('menu', 'visitors')->with('start_date',date("d/m/Y", strtotime($start_date)))->with('end_date',date("d/m/Y", strtotime($end_date)))->with('sub_menu', 'tracking_details')->with('type',$type)->with('sms_balance', $this->sms_balance);

        $visitors = Visitor::where('type','official')->get();
        return view('.admin.tracking_details')->with('visitors',$visitors)->with('menu','tracking_details')->with('sms_balance', $this->sms_balance);
   }
    public function updateTicket(Request $request){
        $request_data = $request->all();
        $messages = [

            'comments_date.required' => 'Please enter a date',
            'comment.required' => 'Please enter comment',
        ];


        $validator = Validator::make($request_data, [
            'comments_date' => 'required|date',
            'comment' => 'required',
        ], $messages);

        if($validator->fails())
        {

            return redirect()->back()->withErrors($validator)->withInput();
        }


        if(!empty($request->comments_date) && !empty($request->comment)){
            $visitor = Visitor::find($request->visitor_id);
            $visitor->status = $request->status;
            $visitor->save();
            $comment = new trackingComent();
            $comment->visitor_id = $request->visitor_id;
            $comment->comments_date = $request->comments_date;
            $comment->comment = $request->comment;
            $comment->save();
            
            Log::info('Message Visitor ID : ' .$visitor->id);  
            Log::info('Message Tracking ID : ' .$visitor->tracking_id);  
            Log::info('Tracking Status : ' .$visitor->status);  
            $contact_numbers = [$visitor->contact_number];  
            $message = 'Your ticket ' . $visitor->tracking_id . ' has been updated\n' ;  
            $message .= 'Message :  ' . $comment->comment;  
            $title = 'Ticket Updates';  
            $full_name = $visitor->full_name;
            $user_email = $visitor->email;
            $tracking_id = $visitor->tracking_id;
            //$response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);  
            $send_data = '';
            if($visitor->status == 'In progress' && $visitor->type == 'official'){
                $jsonBody = json_encode([
                    'template_id' => '656edf2fbb129074e934d3b4',
                    'short_url' => '1',
                    'sender' => 'PRSLAD',
                    'mobiles' => '',
                    'recipients' => [
                        [
                            'name' => $full_name,
                            'trackno' => $tracking_id,
                        ]
                    ]
                ]);
                $send_data = $jsonBody;
            }elseif($visitor->status == 'Hold' && $visitor->type == 'official'){
                $jsonBody = json_encode([
                    'template_id' => '656f0214856ce0037d4925d2',
                    'short_url' => '1',
                    'sender' => 'PRSLAD',
                    'mobiles' => '',
                    'recipients' => [
                        [
                            'name' => $full_name,
                            'trackno' => $tracking_id,
                        ]
                    ]
                ]);
                $send_data = $jsonBody;
            }elseif($visitor->status == 'Closed' && $visitor->type == 'official'){
                $jsonBody = json_encode([
                    'template_id' => '65b89036bd78674ed8198596',
                    'short_url' => '1',
                    'sender' => 'PRSLAD',
                    'mobiles' => '',
                    'recipients' => [
                        [
                            'name' => $full_name,
                            'trackno' => $tracking_id,
                        ]
                    ]
                ]);
                $send_data = $jsonBody;
            }
            // dd($send_data);
            $response_data = $this->home_controller->sendTemplateMessage($contact_numbers, $send_data);  
            Log::info('==================================================');            
            if($user_email != "")
            {
              Mail::send('emails.update_ticket', array('full_name' => $full_name, 'tracking_id' => $tracking_id, 'comment'=> $comment->comment), function($message) use ($user_email, $full_name)
              {
                $message->to($user_email, $full_name)->subject('Ticket Status');
              });
            }
            
        }
        return back()->with('success','Tracking update successfully');

    }
    public function viewTicket(Request $request){
       $ticket = Visitor::find($request->id);
       return view('admin.ticket_view')->with('menu', 'visitors')->with('sub_menu', 'tracking_details')->with('visitor',$ticket)->with('sms_balance', $this->sms_balance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editTicket(Request $request)
    {
        $visitor = Visitor::find($request->id);
        return view('admin.edit_ticket')->with('menu', 'visitors')->with('sub_menu', 'tracking_details')->with('visitor',$visitor)->with('sms_balance', $this->sms_balance);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function visitorDelete(Request $request)
    {

        Visitor::find($request->id)->delete();
        return redirect()->back()->with('success','Deleted successfully');
    }
}
