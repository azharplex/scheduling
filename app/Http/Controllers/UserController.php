<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;

class UserController extends Controller
{
  public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
  
  //User Register
  public function postRegister(Request $request)
  {
    $request_data = $request->all();

    $messages = [
      'email.required' => 'Please enter email',
      'password.required' => 'Please enter password',
      'confirm-password.required' => 'Please enter password',
      'accept-tnc.required' => 'Please accept terms and conditions',
      'name.required' => 'Please enter your name',
    ];
    
    $validator = Validator::make($request_data, [
			'email' => 'required|email|max:255|unique:user',
			'password' => 'required|min:8',
			'accept-tnc' => 'required',
			'name' => 'required|min:3',
      'confirm-password' => 'required|min:8|same:password'         
		], $messages);
    
    if($validator->fails())
    {
      return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
    }
    else
    {
      //0=Admin, 1=Executive, 2=Data monitor User, 3=Video Uploader, 4=Customer Admin, 5=SubCustomer Admin, 6=Customer
      $obj_user = new User;
      $obj_user->name = $request_data['name'];
      $obj_user->email = $request_data['email'];
      $obj_user->password = bcrypt($request_data['password']);
      $obj_user->user_type = 6;   
      $obj_user->status = 1;   
      $obj_user->dob = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', '01/01/1980')));    
      $obj_user->image = 'unknown.jpg';      
      $obj_user->save(); 

      return redirect()->to('/');
    }
  }  
  
  //Login
  public function postLogin(Request $request)
  {
    //dd(Hash::make($request->user_password));
    $request_data = $request->all();
   
    $messages = [
      'email.required' => 'Please enter email',
      'password.required' => 'Please enter password',
    ];
    
    $validator = Validator::make($request_data, [
			'email' => 'required|email|max:255',
			'password' => 'required|min:8' ,
		], $messages);
    
    if($validator->fails())
    {
        return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {
      $email = $request_data['email'];
      $user_details = User::where('email', $email)->first();
      if ($user_details !== null)
      {
        $credentials = array('email'=> $email ,'password'=> $request_data['password']);        
        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
          return redirect()->to('/'); //Redirect on dashboard
        }
        else
        {
          $error = array('password' => 'Please enter a correct password'); 
          return redirect()->back()->withErrors($error)->withInput();; // If password is wrong redirect back on login form
        }
      }
      else
      {
        $error = array('password' => 'User not found');
        return redirect()->back()->withErrors($error)->withInput();;  //Redirect back on login form
      }    
    }
  }
  
  //Logout
  public function getLogout(Request $request)
	{
		$this->auth->logout();
		return redirect('/'); //Redirect to admin login form
	}
}
