<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
  public function __construct(HomeController $home_controller)
  {
    $this->home_controller = $home_controller;
  }
    
  public function getDashboard()
  {
    $sms_balance = $this->home_controller->getSMSBalance();
    $current_date = date('Y-m-d');
    $visitor_date = date("Y-m-d",strtotime("-30 days",strtotime($current_date))); 
    $week_date = date("Y-m-d",strtotime("+7 days",strtotime($current_date))); 

    $total_visitors = DB::table('visitors')->whereDate('created_at', '>=', $visitor_date)->count();
    
    /*$upcoming_birthdays = DB::table('wishes')
    ->whereDay('birth_date', '>=', date('d', strtotime($current_date)))
    ->whereDay('birth_date', '<', date('d', strtotime($week_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($current_date)))
    ->orderBy(DB::raw('Day(birth_date)'))->orderBy(DB::raw('MONTH(birth_date)'))->limit(5)->get();*/
    
    $upcoming_birthdays = DB::select('SELECT * FROM wishes where birth_date != "" AND (MONTH(birth_date) = '. date('m', strtotime($current_date)) .' AND DAYOFMONTH(birth_date) >= '. date('d', strtotime($current_date)) .') AND (MONTH(birth_date) = '. date('m', strtotime($week_date)).' AND DAYOFMONTH(birth_date) < '. date('d', strtotime($week_date)) .') ORDER BY MONTH(birth_date), DAYOFMONTH(birth_date) LIMIT 5');
    
    /*$upcoming_anniversary = DB::table('wishes')
    ->whereDay('anniversary_date', '>=', date('d', strtotime($current_date)))
    ->whereDay('anniversary_date', '<', date('d', strtotime($week_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($current_date)))
    ->orderBy(DB::raw('Day(anniversary_date)'))->orderBy(DB::raw('MONTH(anniversary_date)'))->limit(5)->get();*/
    
    $upcoming_anniversary = DB::select('SELECT * FROM wishes where anniversary_date != "" AND (MONTH(anniversary_date) = '. date('m', strtotime($current_date)) .' AND DAYOFMONTH(anniversary_date) >= '. date('d', strtotime($current_date)) .') AND (MONTH(anniversary_date) = '. date('m', strtotime($week_date)).' AND DAYOFMONTH(anniversary_date) < '. date('d', strtotime($week_date)) .') ORDER BY MONTH(anniversary_date), DAYOFMONTH(anniversary_date) LIMIT 5');
    
    /*$upcoming_appointment = DB::select("SELECT * FROM appointments WHERE DAY(appointment_date) >= ? AND DAY(appointment_date) < ? AND MONTH(appointment_date) = ? ORDER BY appointment_date, STR_TO_DATE(appointment_time, '%h:%i %p') LIMIT 5", [date('d', strtotime($current_date)), date('d', strtotime($week_date)), date('m', strtotime($current_date))]);*/
    
    $upcoming_appointment = DB::select('SELECT * FROM appointments where appointment_date != "" AND (MONTH(appointment_date) = '. date('m', strtotime($current_date)) .' AND DAYOFMONTH(appointment_date) >= '. date('d', strtotime($current_date)) .') AND (MONTH(appointment_date) = '. date('m', strtotime($week_date)).' AND DAYOFMONTH(appointment_date) < '. date('d', strtotime($week_date)) .') ORDER BY MONTH(appointment_date), DAYOFMONTH(appointment_date) LIMIT 5');
    
    $visitor_bar_graph_data = array();
    for($i=5; $i>=0; $i--)
    {
      $new_date = date("Y-m-d",strtotime("-". $i ." month", strtotime($current_date))); 
      $visitors = DB::table('visitors')
      ->whereMonth('date_of_visit', '=', date('m', strtotime($new_date)))
      ->whereYear('date_of_visit', '=', date('Y', strtotime($new_date)))
      ->count();
      
      $appointments = DB::table('appointments')
      ->whereMonth('appointment_date', '=', date('m', strtotime($new_date)))
      ->whereYear('appointment_date', '=', date('Y', strtotime($new_date)))
      ->count();
    
      $obj_visitor_data = new VisitorData();
      $obj_visitor_data->month = date('F', strtotime($new_date));
      $obj_visitor_data->visitor_count = $visitors;
      $obj_visitor_data->appointment_count = $appointments;
      $visitor_bar_graph_data[] = $obj_visitor_data;
    }

    return view('admin.dashboard')
    ->with('menu', 'dashboard')
    ->with('sms_balance', $sms_balance)
    ->with('visitor_count', $total_visitors)
    ->with('visitor_bar_graph_data', $visitor_bar_graph_data)
    ->with('upcoming_appointments', $upcoming_appointment)
    ->with('upcoming_anniversary', $upcoming_anniversary)
    ->with('upcoming_birthdays', $upcoming_birthdays);
  }
}

Class VisitorData{}
