<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Wish;
use App\Appointment;
use App\Message;

use Mail;
use Auth;
use DB;
use Log;

class CronController240822 extends Controller {  
  public function __construct(HomeController $home_controller)
  {
    $this->home_controller = $home_controller;
  }

  //Send Today Wishes [php artisan sendtodaywishes]
  public function SendTodayWishes()
  {
    $current_date = date('Y-m-d');
    
    $birthdays = Wish::whereDay('birth_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($current_date)))->get();
    
    $anniversaries = Wish::whereDay('anniversary_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($current_date)))->get();
    
    $birthday_list = "";
    $anniversary_list = "";
    
    for($i=0; $i<count($birthdays); $i++)
    {
      //$obj_wishes_list->full_name = $birthdays[$i]->full_name;
      //$obj_wishes_list->contact_number = $birthdays[$i]->contact_number;
      //$obj_wishes_list->email = $birthdays[$i]->email;
      $birthday_list .= $birthdays[$i]->full_name . '\n'; // .' - '. $birthdays[$i]->contact_number . '\n';
    }
    
    for($j=0; $j<count($anniversaries); $j++)
    {
      $anniversary_list .= $anniversaries[$j]->full_name . '\n'; // .' - '. $anniversaries[$j]->contact_number . '\n';
    }   
    
    Log::info('Date  : ' .$current_date);  
    Log::info('Wish List : ' .$birthday_list);  
    
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];  
    $message = 'Todays Wish List\n\n';  
    if(count($birthdays) > 0)
    {
      $message .= 'Birthdays :  \n' .$birthday_list;  
    }
    else
    {
      $message .= 'No Birthdays For Today \n';  
    }
    $message .= '~~~~~~~~~~~~~ \n'; 
    if(count($anniversaries) > 0)
    {
      $message .= 'Anniversaries : \n' .$anniversary_list; 
    }
    else
    {
      $message .= 'No Anniversaries For Today \n';  
    }
    $title = 'Todays Wish List';  
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');

    /*Mail::send('emails.todays_wishes', array('birthdays' => $birthdays, 'anniversaries' => $anniversaries), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Todays Wish List');
        }
      }
    });*/
            
    Log::info('Todays Wish List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Tomorrow Wishes [php artisan sendtomorrowwishes]
  public function SendTomorrowWishes()
  {
    $current_date = date('Y-m-d');
    $tomorrow_date = date("Y-m-d",strtotime("+1days",strtotime($current_date)));

    $birthdays = Wish::whereDay('birth_date', '=', date('d', strtotime($tomorrow_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($tomorrow_date)))->get();
    
    $anniversaries = Wish::whereDay('anniversary_date', '=', date('d', strtotime($tomorrow_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($tomorrow_date)))->get();
    
    $birthday_list = "";
    $anniversary_list = "";
    
    for($i=0; $i<count($birthdays); $i++)
    {
      //$obj_wishes_list->full_name = $birthdays[$i]->full_name;
      //$obj_wishes_list->contact_number = $birthdays[$i]->contact_number;
      //$obj_wishes_list->email = $birthdays[$i]->email;
      $birthday_list .= $birthdays[$i]->full_name . '\n'; // .' - '. $birthdays[$i]->contact_number . '\n';
    }
    
    for($j=0; $j<count($anniversaries); $j++)
    {
      $anniversary_list .= $anniversaries[$j]->full_name . '\n'; // .' - '. $anniversaries[$j]->contact_number . '\n';
    }
    
    Log::info('Date  : ' .$current_date);  
    Log::info('Wish List : ' .$birthday_list);  
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];  
    $message = 'Tomorrows Wish List\n\n';  
    if(count($birthdays) > 0)
    {
      $message .= 'Birthdays :  \n' .$birthday_list;  
    }
    else
    {
      $message .= 'No Birthdays For Tomorrow \n';  
    }
    $message .= '~~~~~~~~~~~~~ \n'; 
    if(count($anniversaries) > 0)
    {
      $message .= 'Anniversaries : \n' .$anniversary_list; 
    }
    else
    {
      $message .= 'No Anniversaries For Tomorrow \n';  
    }
    $title = 'Tomorrows Wish List';  
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');
    Mail::send('emails.tomorrows_wishes', array('birthdays' => $birthdays, 'anniversaries' => $anniversaries), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Tomorrows Wish List');
        }
      }
    });
    Log::info('Tomorrows Wish List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Todays Appointment [php artisan sendtodayappointmentlist]
  public function SendTodayAppointmentList()
  {
    $current_date = date('Y-m-d');
    $tomorrow_date = date("Y-m-d",strtotime("+1days",strtotime($current_date)));

    $appointments = Appointment::whereDate('appointment_date', '=', $current_date)->OrderBy(DB::raw("STR_TO_DATE(appointment_time, '%h:%i %p')"), 'ASC')->get();    
    $appointment_list = "";
    
    for($i=0; $i<count($appointments); $i++)
    {
      $appointment_list .= 'Name : '.$appointments[$i]->full_name . '\n'; // .' - '. $appointments[$i]->contact_number . '\n';
      $appointment_list .= 'Purpose Of Visit : '.$appointments[$i]->purpose_of_visit . '\n'; // .' - '. $appointments[$i]->contact_number . '\n';
      $appointment_list .= 'Date Time : '.date('d-F h:i a', strtotime($appointments[$i]->appointment_date .' '. $appointments[$i]->appointment_time)). '\n';
			$appointment_list .= 'Location : '.$appointments[$i]->location . '\n';
			$appointment_list .= ' - - - \n';
    }
    
    Log::info('Date  : ' .$current_date);  
    Log::info('Appointment List : ' .$appointment_list);  
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];   
    $message = 'Todays Appointment List\n\n';  
    if(count($appointments) > 0)
    {
      $message .= '' .$appointment_list;  
    }
    else
    {
      $message .= 'No Appointments For Today \n';  
    }
 
    $title = 'Todays Appointment List'; 
		//$contact_numbers = ['9819629966', '7020478974', '9820748909'];//7020427329

		/*if(strlen($message) > 150)
		{
			Log::info('=================LONG MESSAGE INFO=======================');
			Log::info('Message Length : ' .strlen($message));
			Log::info('Message : ' .$message);
			$words  = explode('- - - \n', $message);
			//Log::info('Message Divided : ');
			//Log::info($words);
			for($sms=0; $sms<count($words); $sms++)
			{
				$a_count = $sms+1;
				$title = "Appointment : $a_count";
				//Log::info('Divided Message Title : '. $title);
				//Log::info('Divided Message Length : '. strlen($words[$sms]));
				//Log::info('Divided Message : ' . $words[$sms]);
				if(strlen($words[$sms]) > 150) 
				{
					$break_sms  = explode('Location', $words[$sms]);
					//Log::info('Explode Location : ');
					//Log::info($break_sms);
					for($bs=0; $bs<count($break_sms); $bs++)
					{			
				    $break_sms_1 = $title .'\n '. $break_sms[$bs];
				    //$break_sms_1 = $break_sms[$bs];
						if($bs == 1)
						{
							$break_sms_1 = $title .'\n Location '. $break_sms[$bs];
						}
				    $break_sms_1_len = strlen($break_sms[$bs]);
						Log::info('Break sms 1 : ' . $break_sms_1);				
						Log::info('Break sms 1 Length : ' . $break_sms_1_len);				
						Log::info('--------------');				
						#print $break_sms_1;
						#print $break_sms_1_len;
						#print '--------------';	
						$response_data = $this->home_controller->sendMessage($contact_numbers, $break_sms_1, $title); 				
					}
				}		
        else
				{				
					Log::info('Send By Appointment : ' . $words[$sms]);			
					$response_data = $this->home_controller->sendMessage($contact_numbers, $words[$sms], $title); 
				}				
			}			
		}
		else
		{
			$response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
		}*/
		
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');
    /*Mail::send('emails.todays_appointment', array('appointments' => $appointments), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Todays Appointment List');
        }
      }
    });*/
    Log::info('Todays Appointment List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Tomorrow Appointment [php artisan sendtomorrowappointmentlist]
  public function SendTomorrowAppointmentList()
  {
    $current_date = date('Y-m-d');
    $tomorrow_date = date("Y-m-d",strtotime("+1days",strtotime($current_date)));

    $appointments = Appointment::whereDate('appointment_date', '=', $tomorrow_date)->OrderBy(DB::raw("STR_TO_DATE(appointment_time, '%h:%i %p')"), 'ASC')->get();  

    $appointment_list = "";
    
    for($i=0; $i<count($appointments); $i++)
    {
			$appointment_list .= 'Name : '.$appointments[$i]->full_name . '\n'; // .' - '. $appointments[$i]->contact_number . '\n';
      $appointment_list .= 'Purpose Of Visit : '.$appointments[$i]->purpose_of_visit . '\n'; // .' - '. $appointments[$i]->contact_number . '\n';
      $appointment_list .= 'Date Time : '.date('d-F h:i a', strtotime($appointments[$i]->appointment_date .' '. $appointments[$i]->appointment_time)). '\n';
			$appointment_list .= 'Location : '.$appointments[$i]->location . '\n';
			$appointment_list .= ' - - - \n';			
    }
    
    Log::info('Date  : ' .$current_date);  
    Log::info('Appointment List : ' .$appointment_list);  
    $admins = $this->getAdminList();
    $contact_numbers = $admins['numbers'];  
    $users = $admins['users'];  
    $message = 'Tomorrows Appointment List\n\n';  
    if(count($appointments) > 0)
    {
      $message .= '' .$appointment_list;  
    }
    else
    {
      $message .= 'No Appointments For Tomorrow \n';  
    }
 
    $title = 'Tomorrows Appointment List';  
    $response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
    Log::info('==================================================');
    Mail::send('emails.tomorrows_appointment', array('appointments' => $appointments), function($message) use ($users)
    {
      foreach($users as $user)
      {
        if($user->email != "")
        {
          $message->to($user->email, $user->name)->subject('Tomorrows Appointment List');
        }
      }
    });
    Log::info('Tomorrows Appointment List Message Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Send Wishes to user [php artisan sendwishes]
  public function SendWishes()
  {
    $current_date = date('Y-m-d');

    $birthdays = Wish::whereDay('birth_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('birth_date', '=', date('m', strtotime($current_date)))->get();
    
    $anniversaries = Wish::whereDay('anniversary_date', '=', date('d', strtotime($current_date)))
    ->whereMonth('anniversary_date', '=', date('m', strtotime($current_date)))->get();
    
    $birthday_message = 'Happy Birthday to you';    
    $anniversary_message = 'Happy Anniversary to you and your wife'; 
      
    $message_data = Message::first(); 
    
    if(count($message_data) > 0)
    {
      $birthday_message = $message_data->birthday_message;    
      $anniversary_message = $message_data->anniversary_message; 
    }          
   
    $b_message = $birthday_message .' \n\nFrom \nMr. Prasad Lad \nMLC & Vice President BJP- Maharastra \n'; 
    
    for($i=0; $i<count($birthdays); $i++)
    {
      $name = $birthdays[$i]->full_name;
      $email = $birthdays[$i]->email;
      $contact_numbers = explode(' / ', $birthdays[$i]->contact_number);
      $title = 'Happy Birthday';  
      $response_data = $this->home_controller->sendMessage($contact_numbers, $b_message, $title); 
      if($email != "")
      {
        Mail::send('emails.birthday_wishes', array('email' => $email, 'name' => $name, 'b_message' => $birthday_message), function($message) use ($email, $name)
        {
          $message->to($email, $name)->subject('Happy Birthday');
        });
      }
      Log::info('Date  : ' .$current_date);  
      Log::info('Birthday email : ' .$email); 
    }
    
    $a_message = $anniversary_message .'\n\nFrom \nMr. Prasad Lad \n'; 
    for($j=0; $j<count($anniversaries); $j++)
    {
      $name = $anniversaries[$j]->full_name;
      $email = $anniversaries[$j]->email;
      $contact_numbers = [$anniversaries[$j]->contact_number];
      $title = 'Happy Anniversary';  
      $response_data = $this->home_controller->sendMessage($contact_numbers, $a_message, $title); 
      if($email != "")
      {
        Mail::send('emails.anniversary_wishes', array('email' => $email, 'name' => $name, 'a_message' => $anniversary_message), function($message) use ($email, $name)
        {
          $message->to($email, $name)->subject('Happy Anniversary');
        });
      }
      Log::info('Date  : ' .$current_date);  
      Log::info('Anniversary email : ' .$email); 
    }       
    Log::info('==================================================');
    Log::info('Wishes Message & Email Send Successfully: '. date('d-m-Y h:i:s'));
  } 
  
  //Get admin list to send mails and sms
  public function getAdminList()
  {
    $users = User::where('id', '!=', 1)->get();
    $numbers = User::where('id', '!=', 1)->lists('number')->toArray();
    return array('numbers' => $numbers, 'users' => $users);
  }
}
