<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Config;
use Carbon;
use Log;
use Hash;
use Validator;
use Mail;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

use App\Visitor;

class APIController extends Controller
{
  public function __construct(Guard $auth, HomeController $home_controller)
  {
    $this->auth = $auth;
    $this->app_url = Config::get('app.url');
		$this->home_controller = $home_controller;
		$this->sms_balance = $this->home_controller->getSMSBalance();
  } 

	//API: http://scheduling.net/api/visits
  //Headers: access-token, {"filter_from_date":"2019-12-01", "filter_to_date":"2020-01-30", "type":"official"}
  //Output: {"data":{"result":"success"},"message":""}
	public function postVisits(Request $request)
	{
    $response = "";
    $error_message = "";
		$data = array();
    $request_header = $request->header();
		$request->headers->set('content-type','application/json');
		$request_data = $request->all();
    $access_token = $request_header['access-token'];
    $chk_access_token = '465be78fc210e72c5694c01865857ee3';
    if($access_token[0] == $chk_access_token)
    {
			$type = "";
			$current_date = date('d-m-Y');
			$start_date = date("d-m-Y",strtotime("-30days",strtotime($current_date)));
			$end_date = date('d-m-Y');
			if(isset($request->type) || isset($request->filter_from_date)){
				if(isset($request->type)){
					$type = $request->type;
				}
				if(isset($request->filter_from_date) && isset($request->filter_to_date)){
					$start_date = $request->filter_from_date;
					$end_date = $request->filter_to_date;
				}
				$start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
				$end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));

				$visitors = Visitor::select('*');
				 if (isset($request->type) && !empty($request->type)){
						 $visitors = $visitors ->where('type',$request->type);
				 }
				 if(isset($request->filter_date)){
					 $date_range = $request->filter_date;
	
					 $visitors = $visitors->where('date_of_visit','>=',date('Y-m-d',strtotime($start_date))) ->where('date_of_visit','<=',date('Y-m-d',strtotime($end_date)));         
				 }
				 $visitors = $visitors->orderBy('date_of_visit', 'desc')->get();
			}else{
				$visitors = Visitor::where('date_of_visit','>=',date('Y-m-d',strtotime($start_date))) ->where('date_of_visit','<=',date('Y-m-d',strtotime($end_date)))->orderBy('date_of_visit', 'desc')->get();  
			}
			$response = ['status' => true, 'data' => $visitors]; 
      return response()->json($response, 200); 
    }
    else
    {
			$error_message = "Please Login";
      $response = ['code' => 100, 'message' => $error_message];  
      return response()->json(['status' => false, 'error' => $response]);
    }
	}
	
	//http://scheduling.net/api/add-visitor
	//{"date_of_visit":"2020-01-21", "name":"Komal", "type":"official", "contact_number":"9898987876", "purpose_date":"सेवेत रुजू करून घेणेबाबत", "description":"सेवेत रुजू करून घेणेबाबत", "email":"komal@pleximus.com"}
	public function postAddVisitor(Request $request) {
    $response = "";
    $error_message = "";
		$data = array();
    $request_header = $request->header();
		$request->headers->set('content-type','application/json');
		$request_data = $request->all();
    $access_token = $request_header['access-token'];
    $chk_access_token = '465be78fc210e72c5694c01865857ee3';
    if($access_token[0] == $chk_access_token)
    {
			try{
				$request_data = $request->all();
				$messages = [

						'name.required' => 'Please enter full name',
						'date_of_visit.required' => 'Please enter date of visit',
						'type.required' => 'Please enter contact number',
						'contact_number.required' => 'Please enter contact number',
						'contact_number.numeric' => 'Contact number must be numeric',
						'contact_number.regex' => 'Contact number must be 10 digits',
						'bdate.required' => 'Please enter birth date',
						'any_date.required' => 'Please enter anniversary date',
						'designition.required' => 'Please enter designation',
						'email.required' => 'Please enter email',
						'purpose_date.required' => 'Please enter purpose of visit',
						'description.required' => 'Please enter work description',
				];

				$validator = Validator::make($request_data, [
						'date_of_visit' => 'required',
						'name' => 'required',
						'type' => 'required',
						'contact_number' => 'numeric|required|regex:/^[0-9]{10}$/',
						//'designition' => 'required',
						'purpose_date' => 'required',
						'description' => 'required',
						'email' => 'email|max:255',
				], $messages);
        
				if($validator->fails())
				{ 
					$errors = $validator->getMessageBag()->toArray();
					foreach($errors as $error)
					{
						$error_message .= $error[0].' ';
					}
					$response = ['code' => 100, 'message' => $error_message];
					return response()->json(['status' => 'Error', 'error' => $response]);
				}

				if(!isset($request->visitor_id)){
					$visitor = new Visitor();
				}else{
					$visitor = Visitor::find($request->visitor_id);
				}

				$visitor->full_name	 = $request->name;
				$visitor->type	 = $request->type;
				$visitor->date_of_visit	 = date('Y-m-d', strtotime($request->date_of_visit)); 
				if($request->type == 'official'){              
					if(!isset($request->visitor_id)){
						$visitor->tracking_id	 = rand(100000,999999);
					}        
				}            
				$visitor->designition	 = isset($request->designition)? $request->designition:'';
				$visitor->contact_number = $request->contact_number;
				$visitor->email	 = $request->email;
				$visitor->department	 = $request->department;
				$visitor->purpose_of_visit	 = $request->purpose_date;
				$visitor->work_description	 = $request->description;
				$visitor->save();
							 
				if(!isset($request->visitor_id)){
					$visitorstatus = Visitor::find($visitor->id);
					if($request->type == 'general'){
						$visitorstatus->status = 'Closed';
					}
					$visitorstatus->save();              
					Log::info('Message Visitor ID : ' .$visitor->id);  
					Log::info('Message Tracking ID : ' .$visitor->tracking_id);  
					$contact_numbers = [$visitor->contact_number];  
					if($request->type == 'official'){
						$message = 'Thank you for Visiting.\n \n Your Tracking ID : ' .$visitor->tracking_id;  
						$track_url = Config('app.url') .'track/schedule'; 
						$message .= '\n \n Kindly use below link to track your ticket \n '. $track_url;  
						$title = 'Tracking ID';  
					}
					else{
						$message = 'Thank you for Visiting Mr. Prasad Lad - MLC & Vice President BJP- Maharastra.';  
						$title = 'Thank you for Visiting'; 
					}
					$response_data = $this->home_controller->sendMessage($contact_numbers, $message, $title);   
					Log::info('==================================================');      
					$full_name = $visitor->full_name;
					$user_email = $visitor->email;
					$tracking_id = $visitor->tracking_id;
					if($user_email != "")
					{
						/*Mail::send('emails.add_visitor', array('full_name' => $full_name, 'tracking_id' => $tracking_id), function($message) use ($user_email, $full_name)
						{
							$message->to($user_email, $full_name)->subject('Thank you for visiting Mr. Prasad Lad');
						});*/
					}
				}
				if(isset($request->visitor_id)){
					if($request->type == 'official'){
						$response = ['data' => $visitor, 'status' => 'Success', 'message' => 'Visitor updated successfully.']; 
					}
				}
				if($request->type == 'official'){
					//return redirect()->back()->with('success','Visitor Update successfully')->with('type','official')->with('name',$request->name)->with('track_id',$visitor->tracking_id);
					$response = ['data' => $visitor, 'status' => 'Success', 'message' => 'Visitor updated successfully.']; 
				}
				$response = ['data' => $visitor, 'status' => 'Success', 'message' => 'Visitor added successfully.']; 
				return response()->json($response, 200);	
			}catch (Exception $exception){
					$response = ['code' => 100, 'message' => $exception];  
					return response()->json(['status' => false, 'error' => $response]);
			}				
			$response = ['status' => true, 'data' => '']; 
      return response()->json($response, 200); 
		}
		else
    {
      $error_message = "Please Login";
      $response = ['code' => 100, 'message' => $error_message];  
      return response()->json(['status' => false, 'error' => $response]);
    }   
	}	
}