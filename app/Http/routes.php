<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use \Illuminate\Support\Facades\Auth;

Route::controllers([
  'user' => 'UserController',
  'admin' => 'AdminController',
  'channel' => 'ChannelController',
  'api' => 'APIController'
]);



Route::get('/visitors/add', 'VisitorController@visitorsAdd');
Route::post('/visitor/save', 'VisitorController@visitorsave');
Route::get('/visitors', 'VisitorController@Visitors');
Route::post('/visitors', 'VisitorController@Visitors');
Route::get('/visitor/view', 'VisitorController@visitorView');

Route::get('/update', 'VisitorController@Update');


Route::post('/tracking/details', 'VisitorController@trackingDetails');
Route::get('/tracking/details', 'VisitorController@trackingDetails');
Route::get('/view/ticket', 'VisitorController@viewTicket');
Route::get('/edit/ticket', 'VisitorController@editTicket');
Route::post('/ticket/delete', 'UserTrackController@trackingDetails');


Route::post('/update/ticket', 'VisitorController@updateTicket');
Route::post('/visitor/delete', 'VisitorController@visitorDelete');

Route::get('/appointments', 'AppointmentController@appointments');
Route::post('/appointments', 'AppointmentController@appointments');
Route::get('/appointment/add', 'AppointmentController@appointmentAdd');
Route::post('/appointment/save', 'AppointmentController@appointmentSave');
Route::get('/appointment/edit', 'AppointmentController@appointmentEdit');
Route::get('/appointment/view', 'AppointmentController@appointmentView');
Route::post('/appoinment/delete', 'AppointmentController@appoinmentDelete');


Route::get('/wish/add', 'WishController@wishAdd');
Route::get('/wish/view', 'WishController@wishView');
Route::post('/wish/save', 'WishController@wishSave');
Route::get('/wishes/users', 'WishController@viewUsers');
Route::get('/wishes/history', 'WishController@viewWishes');
Route::post('/wishes/history', 'WishController@wishesHistory');
Route::post('/wish/delete', 'WishController@wishDelete');
Route::get('/wishes/import', 'WishController@importWishes');
Route::post('/wishes/import', 'WishController@postImportWishes');

Route::get('/track/schedule', 'UserTrackController@index');
//Route::get('public/tracking/details',function (){dd('dddddd');});
Route::post('public/tracking/details', 'UserTrackController@trackingDetails');
Route::post('/tracking/otp', 'UserTrackController@checkOTP');
Route::get('/tracking/otp/{track_id}', 'UserTrackController@getOTP');


Route::get('/settings', 'SettingController@Settings');
Route::post('/settings/save', 'SettingController@SettingsSave');
Route::get('/add/notification', 'SettingController@addNotification');
Route::post('/notification/password', 'SettingController@updatePassword');
Route::post('/notification/save', 'SettingController@notificationSave');
Route::post('/notifaction/delete', 'SettingController@notifactionDelete');

Route::post('/send-mail-text', 'HomeController@postSendMailText');
Route::get('/subscribers', 'HomeController@getSubscribers');
Route::get('/groups', 'HomeController@getGroups');
Route::post('/groups/add', 'HomeController@postAddGroup');
Route::get('/groups/edit', 'HomeController@getEditGroup');
Route::get('/groups/delete', 'HomeController@getDeleteGroup');
Route::get('/email-signature', 'HomeController@getEmailSignature');
Route::get('/emails', 'HomeController@getEmails');
Route::post('/email-signature', 'HomeController@postEmailSignature');
Route::post('/groups/edit', 'HomeController@postEditGroup');
Route::get('/send-mail', 'HomeController@getSendMail');
Route::post('/send-mail', 'HomeController@postSendMail');
Route::post('/send-sms-text', 'HomeController@postSendSMSText');
Route::post('/file-upload', 'HomeController@postFileUpload');
Route::get('/send-sms', 'HomeController@getSendSMS');
Route::post('/send-sms', 'HomeController@postSendSMS');
Route::get('/', 'HomeController@index');

Route::get('/send-appointment-sms', 'HomeController@getAppointmentSendSMS');
Route::post('/send-appointment-sms-text', 'HomeController@postSendAppointmentSMSText');

