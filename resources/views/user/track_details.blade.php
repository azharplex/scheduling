@extends('layouts.header')
<style>
    #mainNav{
        display: none;
    }
</style>
@section('content')
    <section class="content tracking-details">
      <div class="row">
        <div class="col-md-12 header text-center">
          <h2>Tracking Details</h2>
          <h3>{{$visitor->tracking_id}}</h3>
        </div>
      </div>
      <div class="container">
        <div class="row details">
          <div class="col-md-6">
            <table class="table">
              <tr>
                <td width="25%"><label>Type</label></td>  
                <td>
                  @if(!empty($visitor))
                    @if($visitor->type == 'official') 
                      Official
                    @else
                      General
                    @endif
                  @endif
                </td>
              </tr>
              <tr>
                <td><label>Full Name</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->full_name}}
                  @endif
                </td>
              </tr>
              <tr>
                <td><label>Contact Number</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->contact_number}}
                  @endif
                </td>
              </tr>
              <tr>
                <td><label>Email</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->email}}
                  @endif
                </td>
              </tr>
            </table>
          </div>
          <div class="col-md-6">
            <table class="table">
              <tr>
                <td><label>Purpose of Visit</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->purpose_of_visit}}
                  @endif
                </td>
              </tr>
              <tr>
                <td><label>Department</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->department}}
                  @endif
                </td>
              </tr>
              <tr>
                <td><label>Designation</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->designition}}
                  @endif
                </td>
              </tr>
              <tr>
                <td><label>Work Description</label></td>  
                <td>
                  @if(!empty($visitor))
                    {{$visitor->work_description}}
                  @endif
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12 comment-header text-center">
          <h3>Tracking Comments</h3>
        </div>
      </div>
      <div class="container">
        <div class="row details">
          <div class="col-md-12">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th class="text-center">Date</th>
                  <th class="text-center">Comment</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($visitor->getComments[0]))
                  @foreach($visitor->getComments as $comment)
                    <tr>
                      <td class="text-left">{{date('d-m-Y',strtotime($comment->comments_date))}}</td>
                      <td class="text-left">{{$comment->comment}}</td>
                    </tr>
                  @endforeach
                @else
                <tr>
                  <td class="text-center" colspan="2">No Comments</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>         
        </div>
      </div>
    </section>
@endsection
@section('script')

@stop