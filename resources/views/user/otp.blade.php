@extends('layouts.header')
@section('content')

  <header>
    <div class="header-content">
      <div class="header-content-inner">
        <div class="row"> 
          <div class="col-md-6 text-center col-md-offset-3 login-box">    
            {{--{{ Form::open(array('url' => '/user/login')) }}--}}
            <form id="form-sign-in" role="form" method="POST" action="{{ url('/tracking/otp') }}" novalidate>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">           
              <div class="col-md-10 col-md-offset-1">

                <div class="form-group">
                  <input type="text" class="form-control" id="otp" placeholder="Enter OTP" name="otp" value="">
                  <input type="hidden" name="track_id" value="{{ $track_id }}">
                  <span class="error-msgs text-center text-danger">{{ $errors->first('otp') }}</span>
                  @if(!empty(session('error')))
                    <span class="error-msgs text-center text-danger">{{ session('error') }}</span>
                    @endif
                </div>                  
                <div class="form-group">
                  <button class="btn btn-primary" type="submit">Submit</button>
                  <div class="text-center"><span id="sign-in-loading" style="display:none;"><img src="images/loading.gif" alt="" /></div>
                </div>
              </div> 
            </form>
            {{--{{ Form::close() }}--}}            
          </div>
        </div>
      </div>
    </div>
  </header>   
<script>
$(function(){

});
</script>
@endsection