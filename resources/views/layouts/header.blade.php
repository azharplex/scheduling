<!DOCTYPE html>
<html>
<head>
 <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">-->
	<meta content="utf-8" http-equiv="encoding">
  <title>Prasad Lad</title>
  <link rel="shortcut icon" href="/images/favicon.ico">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>	
  <link href="{{ asset('/css/magnific-popup.css') }}" rel="stylesheet">
  
  <!-- Theme CSS -->
	<link href="{{ asset('/css/creative.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/welcome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


  <![endif]-->
	
	<!-- jQuery 2.2.0 -->
	<script src="{{ asset('/js/jquery.min.js') }}"></script>
</head>

<body id="home-page-top">
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand page-scroll" href="{{url('/')}}"><span>Prasad Lad</span></a>
      </div>
      <div class="collapse navbar-collapse text-center" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a  href="{{url('track/schedule')}}"    class="btn-show-login-modal">Track your work</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>    
  @yield('content')
  <div class="main-footer text-center">
    <span>Copyright &copy; 2018</span> All rights reserved. Developed & Maintained by <a href="https://pleximus.com/" target="_blank">Pleximus</a>
  </div>
  <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('/js/jquery.easing.min.js') }}"></script>
  <!-- Datepicker JS -->  
  <script src="{{ asset('/js/bootstrap-datepicker.js') }}"></script>
  <!-- Plugin JavaScript -->
  <script src="{{ asset('/js/scrollreveal.min.js') }}"></script>
  <script src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
  <!-- Theme JavaScript -->
  <script src="{{ asset('/js/creative.min.js') }}"></script>
  <script src="{{ asset('/js/scheduling.js') }}"></script>
  {{--{{dd(session('error'))}}--}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script>

      @if(!empty(session('success')))

      toastr.success('{{session('success')}}')

      @endif
      @if(!empty(session('error')))

      toastr.warning('{{session('error')}}')

      @endif
  </script>
</body>
</html>
