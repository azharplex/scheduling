@extends('layouts.admin_header')
@section('content')
    <style>
        @media (min-width: 600px) {
            .dropdown-menu {
                margin-left: 103px;
            }
        }
    </style>
    <section class="content">

            <div class="row">

                <div class="col-md-12">

                    <div class="box box-danger">
                        <div class="box-header">

                        </div>

                        <div class="box-body">
                            {!! Form::open(array('url' => 'settings/save')) !!}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group"  style="text-align: right" >
                                        <span  value=""  id="recipient-name"><b>Default Birthday Message :</b></span>
                                    </div>

                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="birthday_message" @if(!empty($message)) value="{{$message->birthday_message}}" @else value="Happy Birthday to you"
                                               @endif class="form-control" id="recipient-name">
                                        <span class="error-font text-danger">{{ $errors->first('birthday_message')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group"  style="text-align: right" >
                                        <span  value=""  id="recipient-name"><b>Default Anniversary Message :</b></span>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="anniversary_message" @if(!empty($message)) value="{{$message->anniversary_message}}" @else value="Happy Anniversary to you and your wife"
                                               @endif class="form-control" id="recipient-name">
                                        <span class="error-font text-danger">{{ $errors->first('anniversary_message')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="text-align: right">
                                    <button type="submit"  class="btn btn-primary">
                                        @if(!empty($message))
                                            Change
                                        @else
                                            Save
                                        @endif
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>


            </div>


        <div class="row">
            <div class="box box-danger">
                <div class="box-header">
                    <div class="col-md-12" style="text-align: right">
                        <a  class="btn btn-primary" href="{{url('add/notification')}}">Add Admin Emails</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table-visitor" style="width:100%" class="table table-bordered table-striped">
                                <thead>
                                <th class="text-center" style="width:25%"> Name</th>
                                <th class="text-center" style="width:25%"> Email</th>
                                <th class="text-center" style="width:25%"> Number</th>
                                <th class="text-center" style="width:25%"> Action</th>
                                </thead>
                                <tbody>
                                @foreach($notifactions as $notifaction)
                                <tr>

                                    <td class="text-center"> {{$notifaction->name}}</td>
                                    <td class="text-center">{{$notifaction->email}}</td>
                                    <td class="text-center">{{$notifaction->number}}</td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                <span class="caret"></span></button>
                                               <ul class="dropdown-menu">

                                                <li><a href="{{url('add/notification').'?id='.$notifaction->id}}">Edit</a></li>
                                                <li><a href="#" data-toggle="modal" data-target=".delete_{{$notifaction->id}}">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <!-- ========== DELETE MODEL START ========== -->
                                <div class="modal fade delete_{{$notifaction->id}}" id="delete-channel-modal "  role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-md modal-teal">
                                        <div class="modal-content">
                                            <form id="form-delete-channel" role="form" method="POST" action="{{ url('/notifaction/delete') }}" novalidate>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h2 class="modal-title text-center color-skyblue">Notification</h2>
                                                </div>
                                                <input type="hidden" name="id" value="{{$notifaction->id}}">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-10 col-md-offset-1">
                                                            <div class="text-center">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="hidden" id="channel-id" name="channel-id">
                                                                <h4>Are you sure to delete this Notification?</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-4">
                                                            <input type="submit" class="btn btn-primary" value="Yes">
                                                            <a href="/" class="btn btn-primary" data-dismiss="modal">No</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>

@endsection