@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Contact To Send Appointment SMS</h3>
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-9">
                                <form id="form-import-excel-contact" role="form" method="POST"
                                    action="{{ url('/send-sms') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="excel-file">Import excel file</label>
                                                <!--label id="csv-file" class="button text-info" ><span><i class="fa fa-file fa-1x text-info"></i>&nbsp;&nbsp;Select File</span-->
                                                <input type="file" class="btn btn-sm excel-file ml-3"
                                                    name="excel-file" />
                                                <!--/label--><br>
                                                <span
                                                    class="error-font text-danger">{{ $errors->first('excel-error') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-3 text-right">
                                <a href="/images/contacts.xlsx" download="contacts_excel"
                                    class="btn btn-sm btn-danger">Download Sample Excel Sheet</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Send Appointment SMS</h3>
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form id="form-import-excel-contact" role="form" method="POST" action="{{ url('/send-appointment-sms-text') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name :</label>
                                                <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="{{ old('name') }}">
                                                <span class="error-font text-danger">{{ $errors->first('name')}}</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Purpose Of Visit :</label>
                                                <input type="text" class="form-control" id="purpose" placeholder="Purpose Of Visit" name="purpose_of_visit" value="{{ old('purpose_of_visit') }}">
                                                <span class="error-font text-danger">{{ $errors->first('purpose_of_visit')}}</span>
                                            </div>
                                            {{--  <div class="form-group">
                                                <label>Date Time :</label>
                                                <input type="date" class="form-control" id="date" name="date" value="{{ old('date') }}">
                                                <span class="error-font text-danger">{{ $errors->first('date')}}</span>
                                                <br>
                                                <input type="time" class="form-control" id="time" name="time" value="{{ old('time') }}">
                                                <span class="error-font text-danger">{{ $errors->first('time')}}</span>
                                            </div>  --}}

                                            <div class="form-group">
                                                <label for="date" class="control-label">Date :</label>
                                                <div class="input-group date appointment_date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input readonly type="text" class="form-control pull-right" id="date" name="date" value="{{ old('date') }}">
                                                </div>
                                                <span class="error-font text-danger">{{ $errors->first('date')}}</span>
                                            </div>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label>Time :</label>
                                                    <div class="input-group">
                                                        <input type="text" name="time" class="form-control timepicker" id="time" value="{{ old('time') }}">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <span class="error-font text-danger">{{ $errors->first('time')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Location :</label>
                                                <input type="text" class="form-control" id="location" placeholder="Enter Location" name="location" value="{{ old('location') }}">
                                                <span class="error-font text-danger">{{ $errors->first('location')}}</span>
                                            </div>
                                            <h4><b>Monitored by Prasad Lad.</b></h4>
                                        </div>
                                        
                                        <input type="hidden" name="contacts" value="{{ isset($contacts) ? $contacts : old('contacts') }}" />
                                        <input type="hidden" name="contactnames" value="{{ isset($contactnames) ? $contactnames : old('contactnames') }}" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">Send SMS</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-3 text-center">
                                <label>Contact List {{ isset($total_contacts) ? $total_contacts : '' }}</label>
                                <ul class="list-group">
                                    @if (isset($contacts))
                                        <?php
                                        $array_contacts = json_decode($contacts);
                                        $array_contactnames = json_decode($contactnames);
                                        ?>
                                        @foreach ($array_contacts as $key => $contact)
                                            <li class="list-group-item">{{ $array_contactnames[$key] }} -
                                                {{ $contact }}</li>
                                        @endforeach
                                    @elseif(old('contacts'))
                                        <?php
                                        $array_contacts = json_decode(old('contacts'));
                                        $array_contactnames = json_decode(old('contactnames'));
                                        ?>
                                        @foreach ($array_contacts as $key => $contact)
                                            <li class="list-group-item">{{ $array_contactnames[$key] }} -
                                                {{ $contact }}</li>
                                        @endforeach
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script language=JavaScript>
        function check_length(sms_form) {
            var text_count = sms_form.message.value.length;
            sms_form.text_num.value = text_count;
        }
    </script>

    <script>
    document.addEventListener('DOMContentLoaded', function() {
        var dateInput = document.getElementById('date');

        dateInput.addEventListener('input', function() {
            var selectedDate = new Date(dateInput.value + 'T00:00:00');

            var currentDate = new Date();

            var minDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);

            if (selectedDate < minDate) {
                dateInput.value = '';
                alert('Please select a date in the current month or later.');
            }
        });
    });

</script>
<!-- bootstrap time picker -->
    <script src="{{ asset('/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
           
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
    </script>


@endsection
