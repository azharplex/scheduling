{{--{{dd($wish)}}--}}

@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Wish Details</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <?php
                            $now = time(); // or your date as well
                            $date = date('Y-m-d'); // or your date as well
                                $any_date = $wish->anniversary_date;
                                $this_year_any_date = date('Y',strtotime($date)).'-'.date('m-d',strtotime($any_date));
                            $your_date = strtotime($this_year_any_date);

                            $datediff = $now - $your_date;
                            $day = round($datediff / (60 * 60 * 24));
                            $text = '';
                            if ($day == 0) {
                                $text = "Today";
                            } elseif ($day == 1) {
                                $text = 'Open from '. $day . ' day';
                            } else {
                                $text = 'Open from '.$day . ' days';
                            }
                            ?>
                            <div class="col-md-12">
                                <h4><label style="width:100px; font-weight: 500;">NAME </label><b>: {{$wish->full_name}}</b></h4>
                                <h4> <b>{{$wish->full_name}}s  birthday is on {{date('dS',strtotime($wish->birth_date))}} {{date('F',strtotime($wish->birth_date))}}</b> </h4>
                                <h4> <b>{{$wish->full_name}}s  Anniversay is coming in {{$text}}</b> </h4>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <table id="table-visitor" style="width:100%" class="table table-bordered table-striped">

                                    <tbody>
                                    <tr>
                                        <th class="text-left">Anniversary date</th>
                                        <td class="text-left">{{$wish->anniversary_date}}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-left">Birth date</th>
                                        <td class="text-left">{{$wish->birth_date}}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-left">Email</th>
                                        <td class="text-left">{{$wish->email}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Contact number</th>
                                        <td class="text-left">{{$wish->contact_number}}</td>

                                    </tr>


                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection