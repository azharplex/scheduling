@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upcoming Dates For Wishes</h3>      
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(array('url' => 'wishes/history')) !!}
                                <div class="col-md-3 col-sm-3">
                                  <div class="input-group date ">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" readonly id="date_renger" class="form-control pull-right"   name="filter_date" >
                                  </div>
                                </div>
                                {{--<div class="col-md-2 col-sm-3">--}}

                                    {{--<select name="type" class="form-control" id="">--}}
                                        {{--<option value="">All</option>--}}
                                        {{--<option @if(!empty($type) && $type == 'Birth day') selected @endif value="Birth day">Birth day</option>--}}
                                        {{--<option @if(!empty($type) && $type == 'official') selected @endif value="official">Anniversary date</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-2 col-sm-3">--}}

                                    {{--<select name="date" class="form-control" id="">--}}
                                        {{--<option value="">Select</option>--}}
                                        {{--<option @if(!empty($type) && $type == 'Today') selected @endif value="Today">Today</option>--}}
                                        {{--<option @if(!empty($type) && $type == 'Tomorrow') selected @endif value="Today">Tomorrow</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                <div class="col-md-1">
                                    <button type="submit" class="btn btn-primary">Filter</button>
                                </div>
                                {!! Form::close() !!}

                                <table style="width:100%" id="table-visitor" class="table table-bordered table-striped">
                                    <thead>
                                    <tr style="height: 45px">
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Contact Number</th>
                                        <th class="text-center">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($wishes as $wishe)

                                        <tr style="height: 35px">
                                            <td class="text-center">{{ date('d F', strtotime($wishe->date)) }}</td>
                                            <td class="text-center">{{$wishe->full_name}}</td>
                                            <td class="text-center">{{$wishe->type}}</td>
                                            <td class="text-center">{{$wishe->contact_number}}</td>
                                            <td class="text-center">{{$wishe->email}}</td>
                                        </tr>
                                        <!-- ========== DELETE MODEL START ========== -->
                                        {{--<div class="modal fade delete_{{$wishe->id}}" id="delete-channel-modal "  role="dialog" aria-hidden="true">--}}
                                            {{--<div class="modal-dialog modal-md modal-teal">--}}
                                                {{--<div class="modal-content">--}}
                                                    {{--<form id="form-delete-channel" role="form" method="POST" action="{{ url('/visitor/delete') }}" novalidate>--}}
                                                        {{--<div class="modal-header">--}}
                                                            {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                                                            {{--<h2 class="modal-title text-center color-skyblue">Wish</h2>--}}
                                                        {{--</div>--}}
                                                        {{--<input type="hidden" name="id" value="{{$wishe->id}}">--}}
                                                        {{--<div class="modal-body">--}}
                                                            {{--<div class="row">--}}
                                                                {{--<div class="col-md-10 col-md-offset-1">--}}
                                                                    {{--<div class="text-center">--}}
                                                                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                                                        {{--<input type="hidden" id="channel-id" name="channel-id">--}}
                                                                        {{--<h4>Are you sure to delete this visitor?</h4>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="modal-footer">--}}
                                                            {{--<div class="row">--}}
                                                                {{--<div class="col-md-8 col-md-offset-4">--}}
                                                                    {{--<input type="submit" class="btn btn-primary" value="Yes">--}}
                                                                    {{--<a href="/" class="btn btn-primary" data-dismiss="modal">No</a>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</form>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <!-- ========== DELETE MODEL END ========== -->

                                    @endforeach
                                    @foreach($anniversarys as $anniversary)
                                        <tr style="height: 35px">
                                            <td class="text-center">{{ date('d F', strtotime($anniversary->date)) }}</td>
                                            <td class="text-center">{{$anniversary->full_name}}</td>
                                            <td class="text-center">{{$anniversary->type}}</td>
                                            <td class="text-center">{{$anniversary->contact_number}}</td>
                                            <td class="text-center">{{$anniversary->email}}</td>
                                        </tr>
                                        <!-- ========== DELETE MODEL START ========== -->
                                        <div class="modal fade delete_{{$anniversary->id}}" id="delete-channel-modal "  role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-teal">
                                                <div class="modal-content">
                                                    <form id="form-delete-channel" role="form" method="POST" action="{{ url('/wish/delete') }}" novalidate>
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h2 class="modal-title text-center color-skyblue">Wish</h2>
                                                        </div>
                                                        <input type="hidden" name="id" value="{{$anniversary->id}}">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="text-center">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <input type="hidden" id="channel-id" name="channel-id">
                                                                        <h4>Are you sure to delete this wish?</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row">
                                                                <div class="col-md-8 col-md-offset-4">
                                                                    <input type="submit" class="btn btn-primary" value="Yes">
                                                                    <a href="/" class="btn btn-primary" data-dismiss="modal">No</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ========== DELETE MODEL END ========== -->

                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
    <script src="{{asset('/date-range/moment.js')}}" ></script>

    <script>
      $('#date_renger').daterangepicker({
        locale: {
          format: 'DD/MM/YYYY'
        },
        "startDate": "{{$start_date}}",
        "endDate": "{{$end_date}}",
      }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY') + ' (predefined range: ' + label + ')");
      });
    </script>
    <script type="text/javascript">
        $(function(){
            $('#table-visitor').dataTable( {
                "bLengthChange": false,
                "iDisplayLength": 20,
                "infoEmpty": "<center><div class='text-info'><br>No visitor available</div></center>",
                "oLanguage": {
                    "sEmptyTable":"<center><div class='text-info'><br>No visitor available</div></center>",
                    "sSearch": "",
                    "oPaginate": {
                        "sNext": '>',
                        "sLast": '>|',
                        "sFirst": '|<',
                        "sPrevious": '<'
                    }
                },
                "bSort" : false
            });
            $('.dataTables_filter input').attr("placeholder", "Search");
            $('.dataTables_filter input').removeClass("input-sm");
            $('.dataTables_filter input').addClass("form-control");
            $("#table-channel_info").detach().appendTo('#page-link-wrapper');
            $("#table-channel_paginate").detach().appendTo('#page-link-wrapper');
            $("#table-channel_filter").detach().appendTo('#search-channel');

            //Delete channel
            $(document).on('click', '.delete-channel-link', function(ev) {
                ev.preventDefault();
                var channel_id = $(this).data('channel-id');
                $('#channel-id').val(channel_id);
                $('#delete-channel-modal').modal('show');
            });
        });
    </script>
@endsection