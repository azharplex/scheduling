<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview <?php if (isset($menu) && $menu == 'dashboard') {echo "active";} ?>">
                <a href="/">
                    <i class="fa fa-tachometer"></i>
                    <span>Dashboard ( डॅशबोर्ड )</span>
                </a>
            </li>
            <li class="treeview <?php if (isset($menu) && $menu == 'visitors') {echo "active";} ?>">
                <a href="#">
                    <i class="fa fa-users"></i><span>Visitor ( अभ्यागत )</span><i class="fa fa-angle-right pull-right"></i>
                    <ul class="treeview-menu <?php if (isset($menu) && $menu == 'visitors') {echo "menu-open";} ?>">
                        <li class="<?php if (isset($sub_menu) && $sub_menu == 'view_visitors') {echo "active";} ?>"><a href="{{ url('/visitors') }}"><i class="fa fa-table text-white"></i>View Visitor <br/> ( पाहुणा पहा )</a></li>
                        <li class="<?php if (isset($sub_menu) && $sub_menu == 'tracking_details') {echo "active";} ?>"><a href="{{ url('/tracking/details') }}"><i class="fa fa-table text-white"></i>Tracking Details <br/> ( ट्रॅकिंग तपशील )</a></li>
                        <li class="<?php if (isset($sub_menu) && $sub_menu == 'add_visitors') {echo "active";} ?>"><a href="{{ url('/visitors/add') }}"><i class="fa fa-plus text-white"></i>Add Visitor <br/> ( अभ्यागत जोडा )</a></li>
                    </ul>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="treeview <?php if (isset($menu) && $menu == 'appointments') {echo "active";} ?>">
                <a href="#">
                    <i class="ion ion-clipboard"></i><span>Appointment ( नियुक्ती )</span><i class="fa fa-angle-right pull-right"></i>

                    <ul class="treeview-menu <?php if (isset($menu) && $menu == 'appointments') {echo "menu-open";} ?>">

                        <li class="<?php if (isset($sub_menu) && $sub_menu == 'appointments') {echo "active";} ?>"><a href="{{ url('/appointments') }}"><i class="fa fa-table text-white"></i>View Appointments <br/> ( नियोजित भेट पहा )</a></li>
                        <li class="<?php if (isset($sub_menu) && $sub_menu == 'appointment/add') {echo "active";} ?>"><a href="{{ url('/appointment/add') }}"><i class="fa fa-plus text-white"></i>Add Appointment <br/> ( नियोजित जोडा )</a></li>
                    </ul>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="treeview <?php if (isset($menu) && $menu == 'wishes') {echo "active";} ?>">
              <a href="#">
                <i class="fa fa-user"></i><span>Users ( वापरकर्ते ) </span><i class="fa fa-angle-right pull-right"></i>
                <ul class="treeview-menu <?php if (isset($menu) && $menu == 'wishes') {echo "menu-open";} ?>">  
                  <li class="<?php if (isset($sub_menu) && $sub_menu == 'view_wishes') {echo "active";} ?>"><a href="{{ url('/wishes/history') }}"><i class="fa fa-gift text-white"></i>View Wishes ( शुभेच्छा पहा )</a></li>
                  <li class="<?php if (isset($sub_menu) && $sub_menu == 'view_users') {echo "active";} ?>"><a href="{{ url('/wishes/users') }}"><i class="fa fa-table text-white"></i>View Users ( वापरकर्ते पहा )</a></li>
                  <li class="<?php if (isset($sub_menu) && $sub_menu == 'add_wishes') {echo "active";} ?>"><a href="{{ url('/wish/add') }}"><i class="fa fa-plus text-white"></i>Add User ( वापरकर्ते जोडा )</a></li>
                  <li class="<?php if (isset($sub_menu) && $sub_menu == 'import_wishes') {echo "active";} ?>"><a href="{{ url('/wishes/import') }}"><i class="fa fa-download text-white"></i>Import Users ( आयात वापरकर्ते )</a></li>
                </ul>
                <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
            <li class="treeview <?php if (isset($menu) && $menu == 'send_sms') {echo "active";} ?>">
                <a href="{{url('send-sms')}}">
                    <i class="fa fa-envelope  "></i>
                    <span>Send SMS ( एसएमएस पाठवा )</span>
                </a>
            </li>
            <li class="treeview <?php if (isset($menu) && $menu == 'send_appointment_sms') {echo "active";} ?>">
                <a href="{{url('send-appointment-sms')}}">
                    <i class="fa fa-envelope  "></i>
                    <span>Send Appointment SMS <br>( अपॉइंटमेंट एसएमएस पाठवा )</span>
                </a>
            </li>
						<li class="treeview <?php if (isset($menu) && $menu == 'send_mail') {echo "active";} ?>">
                <a href="{{url('send-mail')}}">
									<i class="fa fa-envelope  "></i>
									<span>Send Mail ( मेल पाठवा )</span>
									<i class="fa fa-angle-right pull-right"></i>
									<ul class="treeview-menu <?php if (isset($menu) && $menu == 'send_mail') {echo "menu-open";} ?>">  
										<li class="<?php if (isset($sub_menu) && $sub_menu == 'subscribers') {echo "active";} ?>"><a href="{{ url('/subscribers') }}"><i class="fa fa-users text-white"></i>View Subscribers ( सदस्य पहा )</a></li>
										<li class="<?php if (isset($sub_menu) && $sub_menu == 'groups') {echo "active";} ?>"><a href="{{ url('/groups') }}"><i class="fa fa-cubes text-white"></i>View Groups ( गट पहा )</a></li>
										<li class="<?php if (isset($sub_menu) && $sub_menu == 'mail_signature') {echo "active";} ?>"><a href="{{ url('/email-signature') }}"><i class="fa fa-envelope text-white"></i>Email Signature ( मेल सही )</a></li>
										<li class="<?php if (isset($sub_menu) && $sub_menu == 'send_mail') {echo "active";} ?>"><a href="{{ url('/send-mail') }}"><i class="fa fa-envelope text-white"></i>Send Mail ( मेल पाठवा )</a></li>
										<li class="<?php if (isset($sub_menu) && $sub_menu == 'emails') {echo "active";} ?>"><a href="{{ url('/emails') }}"><i class="fa fa-envelope text-white"></i>Mails Sent ( मेल पाठविले )</a></li>
									</ul>
									<i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="treeview <?php if (isset($menu) && $menu == 'settings') {echo "active";} ?>">
                <a href="{{url('settings')}}">
                    <i class="fa fa-gear"></i>
                    <span>Settings ( सेटिंग्ज )</span>
                </a>
            </li>    
						<li class="treeview <?php if (isset($menu) && $menu == 'settings') {echo "active";} ?>">
								<a href="{{url('user/logout')}}"><i class="fa fa-lock"></i><span>Log Out ( बाहेर पडणे )</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>