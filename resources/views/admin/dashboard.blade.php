@extends('layouts.admin_header')
@section('content')
   <section class="content"> 
		<!--div class="row">
      <div class="col-md-3">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{ $visitor_count }}</h3>
            <p>Last 30 Days Visitors</p>
          </div>
          <div class="icon">
            <i class="fa fa-building"></i>
          </div>
          <a href="/visitors" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>  
      <div class="col-md-3">
        <div class="small-box bg-green">
          <div class="inner">
            <h3>234</h3>
            <p>Total Users</p>
          </div>
          <div class="icon">
            <i class="fa fa-user"></i>
          </div>
          <a href="/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-md-3">
        <div class="small-box bg-maroon">
          <div class="inner">
            <h3>3</h3>
            <p>Today Notifications</p>
          </div>
          <div class="icon">
            <i class="fa fa-user"></i>
          </div>
          <a href="/notifications" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-md-3">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>6</h3>
            <p>Today Appointments</p>
          </div>
          <div class="icon">
            <i class="fa fa-user"></i>
          </div>
          <a href="/appointments" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div--> 
    <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Visitors</h3>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="visitor-bar-chart" style="height:230px"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Appointments</h3>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="appointment-bar-chart" style="height:230px"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 upcoming-box">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><span class="text-info">Upcoming Birthdays</span><span class="text-muted"> [Next 7 days]</span></h3>
          </div>
          <div class="box-body">
            @if(count($upcoming_birthdays) > 0)
            <ul class="products-list product-list-in-box">
              @foreach($upcoming_birthdays as $upcoming_birthday)
              <li class="item">
                <div class="product-img">
                  <img src="/images/cake.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <label class="text-primary">{{ $upcoming_birthday->full_name }}</label>
                  <span class="product-description">
                    {{ date('d M', strtotime($upcoming_birthday->birth_date)) }}
                  </span>
                </div>
              </li>
              @endforeach
            </ul>
            @else
              <label>No Birthdays</label>
            @endif
          </div>
          <!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="/wishes/history" class="uppercase">View All Birthday</a>
          </div>
          <!-- /.box-footer -->
        </div>
      </div>      
      <div class="col-md-4 upcoming-box">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><span class="text-info">Upcoming Anniversaries</span><span class="text-muted"> [Next 7 days]</span></h3>
          </div>
          <div class="box-body">
            @if(count($upcoming_anniversary) > 0)
            <ul class="products-list product-list-in-box">
              @foreach($upcoming_anniversary as $upcoming_anni)
              <li class="item">
                <div class="product-img">
                  <img src="/images/wedding_cake.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <label class="text-primary">{{ $upcoming_anni->full_name }}</label>
                  <span class="product-description">
                    {{ date('d M', strtotime($upcoming_anni->anniversary_date)) }}
                  </span>
                </div>
              </li>
              @endforeach
            </ul>
            @else
              <label>No Anniversaries</label>
            @endif
          </div>
          <!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="/wishes/history" class="uppercase">View All Anniversary</a>
          </div>
          <!-- /.box-footer -->
        </div>
      </div>
      <div class="col-md-4 upcoming-box">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><span class="text-info">Upcoming Appointments</span><span class="text-muted"> [Next 7 days]</span></h3>
          </div>
          <div class="box-body">
            @if(count($upcoming_appointments) > 0)
            <ul class="products-list product-list-in-box">
              @foreach($upcoming_appointments as $appointment)
              <li class="item">
                <div class="product-img">
                  <img src="/images/meeting.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <label class="text-primary">{{ $appointment->full_name }}</label>
                  <span class="product-description">
                   {{ date('d M Y', strtotime($appointment->appointment_date)) }} at {{ date('h:i A', strtotime($appointment->appointment_time)) }}
                  </span>
                </div>
              </li>
              @endforeach
            </ul>
            @else
              <label>No Appointments</label>
            @endif
          </div>
          <!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="/appointments" class="uppercase">View All Appointments</a>
          </div>
          <!-- /.box-footer -->
        </div>
      </div>
    </div>
  </section>
  <script>
    $(function(){

      var visitorChartData = {
        labels: [
          @foreach($visitor_bar_graph_data as $data)
            '{{ $data->month }}',  
          @endforeach
        ],
        datasets: [
          {
            label: "Visitors",
            fillColor: "rgba(174, 214, 241, 1)",
            strokeColor: "rgba(174, 214, 241, 1)",
            pointColor: "rgba(174, 214, 241, 1)",
            pointStrokeColor: "#AED6F1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(93, 173, 226, 1)",
            data: [
              @foreach($visitor_bar_graph_data as $data)
                '{{ $data->visitor_count }}',  
              @endforeach          
            ]
          },
        ]
      };
    
      var visitorBarChartCanvas = $("#visitor-bar-chart").get(0).getContext("2d");
      var visitorBarChart = new Chart(visitorBarChartCanvas);
      var barChartData = visitorChartData;
      //barChartData.datasets[0].fillColor = "#00a65a";
      //barChartData.datasets[0].strokeColor = "#00a65a";
      //barChartData.datasets[0].pointColor = "#00a65a";
      var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barChartOptions.datasetFill = false;
      visitorBarChart.Bar(barChartData, barChartOptions);
      
      var appointmentChartData = {
        labels: [
          @foreach($visitor_bar_graph_data as $data)
            '{{ $data->month }}',  
          @endforeach
        ],
        datasets: [
          {
            label: "Appointments",
            fillColor: "rgba(210,180,222,1)",
            strokeColor: "rgba(210,180,222,1)",
            pointColor: "#D2B4DE",
            pointStrokeColor: "rgba(210,180,222,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(210,180,222,1)",
            data: [
              @foreach($visitor_bar_graph_data as $data)
                '{{ $data->appointment_count }}',  
              @endforeach
            ]
          },
        ]
      };
    
      var appointmentBarChartCanvas = $("#appointment-bar-chart").get(0).getContext("2d");
      var appointmentBarChart = new Chart(appointmentBarChartCanvas);
      var barChartData = appointmentChartData;
      //barChartData.datasets[0].fillColor = "#00a65a";
      //barChartData.datasets[0].strokeColor = "#00a65a";
      //barChartData.datasets[0].pointColor = "#00a65a";
      var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barChartOptions.datasetFill = false;
      appointmentBarChart.Bar(barChartData, barChartOptions);
    });
  </script>
@endsection