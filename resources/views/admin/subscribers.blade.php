@extends('layouts.admin_header')
@section('content')
  <section class="content user-container">
    <div class="row">
			<div class="col-md-12">
				<div class="box box-danger">
					<div class="box-header with-border">
							<h3 class="box-title">Import Email To Send Mail</h3>      
							<div id="search-appointment" class="pull-right"></div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<label>Groups</label>
							</div>
						</div>
						<div class="row">
							<form id="form-import-excel-email" role="form" method="POST" action="{{ url('/send-mail') }}" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
								<div class="col-md-12 from-group">
									@foreach($groups as $group_id => $group)
										<input value="{{ $group_id }}" name="all-groups[]" type="hidden">
										<label class="col-md-2">
											<input value="{{ $group_id }}" name="group[]" type="checkbox">&nbsp;&nbsp;{{ $group }}
										</label>
									@endforeach
								</div>
								<div class="col-md-9">
									<br/>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="excel-file">Import excel file</label>
												<!--label id="csv-file" class="button text-info" ><span><i class="fa fa-file fa-1x text-info"></i>&nbsp;&nbsp;Select File</span--> 
													<input type="file" class="btn btn-sm excel-file ml-3" name="excel-file" />
												<!--/label--><br>
												<span class="error-font text-danger">{{ $errors->first('excel-error')}}</span>
											</div>   
										</div>          
									</div>           
									<div class="row">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">Import</button>
										</div>
									</div>
								</div>
							</form>  
							<div class="col-md-3 text-right">
								<a href="/images/emails.xlsx" download="emails_excel" class="btn btn-sm btn-danger">Download Sample Excel Sheet</a>
							</div>                        
						</div>
					</div>
				</div>
			</div>
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Subscribers</h3>
            <div id="search-user" class="pull-right"></div>
          </div>
          <div class="box-body">
						<table id="table-users" class="table table-bordered table-striped">
							<thead>
								<th>Name</th>
								<th>Email</th>
								<th>Group</th>
							</thead>
							<tbody>
								@foreach($members as $email => $member)
									<tr>
										<td>{{ $member['fname'].' '.$member['lname'] }}</td>
										<td>{{ $email }}</td>
										<td>
											@foreach($member['groups'] as $group_id => $member_group)
												@if($member_group == true && isset($groups[$group_id]))
													<span class="label-primary">&nbsp;&nbsp;{{ $groups[$group_id] }}&nbsp;&nbsp;</span>
												@endif
											@endforeach
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
            <div class="row">
              <div id="page-link-wrapper" class="col-md-12 text-center"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>  
  </section>
  
  <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
  <script src="{{asset('/date-range/moment.js')}}" ></script>
  <script type="text/javascript">
		$(function(){
			$('#table-users').dataTable( {
				"bLengthChange": false,
				"iDisplayLength": 15,
				"infoEmpty": "<center><div class='text-info'><br>No user available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No user available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : false
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			$("#table-users_info").detach().appendTo('#page-link-wrapper');
			$("#table-users_paginate").detach().appendTo('#page-link-wrapper');
			$("#table-users_filter").detach().appendTo('#search-user');

      $('.filter').on('click',function(){
        var filter_date = $(".date_renger").val();     
        var type = $('.select-type').val();     
        window.location.href = '/wishes/users?type='+type+'&filter_date='+filter_date;
      }); 
		});
	</script>
@endsection