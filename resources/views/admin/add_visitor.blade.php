@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">@if(!empty($visitor))Edit @else Add @endif Visitor</h3>
                    </div>
                    <div class="box-body">
                      <div class="row"> 
                       {!! Form::open(array('url' => 'visitor/save')) !!}
                        @if(!empty($visitor))
                            <input type="hidden" name="visitor_id" value="{{$visitor->id}}">
                        @endif
                        
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Type * </label>
                                  @if(!empty($visitor))
                                  <input type="text" value="{{ ucwords($visitor->type) }}" class="form-control" id="type" readonly>
                                  <input type="hidden" name="type" value="{{$visitor->type}}">
                                  @else
                                    <select name="type" id="type" class="form-control">     
                                      <option value="official">Official</option>
                                      <option value="general">General</option>
                                    </select>
                                  @endif
                                  <span class="error-font text-danger">{{ $errors->first('type')}}</span>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Date of Visit * </label>
                                  <div class="input-group date visit_date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input readonly type="text" class="form-control pull-right" name="date_of_visit" @if(!empty($visitor)) value="{{$visitor->date_of_visit}}" @else value="{{ old('date_of_visit') }}" @endif>
                                  </div>
                                  <span class="error-font text-danger">{{ $errors->first('date_of_visit')}}</span>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Full Name * </label>
                                  <input type="text" name="name" @if(!empty($visitor)) value="{{$visitor->full_name}}" @else value="{{ old('name') }}"
                                         @endif class="form-control" id="recipient-name">
                                  <span class="error-font text-danger">{{ $errors->first('name')}}</span>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Contact Number * </label>
                                  <input type="number" name="contact_number"
                                         @if(!empty($visitor)) value="{{$visitor->contact_number}}" @else value="{{ old('contact_number') }}"
                                         @endif class="form-control" id="recipient-name">
                                  <span class="error-font text-danger">{{ $errors->first('contact_number')}}</span>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Email </label>
                                  <input type="email" name="email" @if(!empty($visitor)) value="{{$visitor->email}}" @else value="{{ old('email') }}"
                                         @endif class="form-control" id="recipient-name">
                                  <span class="error-font text-danger">{{ $errors->first('email')}}</span>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="recipient-name" class="control-label">Purpose of Visit * </label>
                                <input type="text" name="purpose_date"
                                       @if(!empty($visitor)) value="{{$visitor->purpose_of_visit}}" @else value="{{ old('purpose_date') }}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('purpose_date')}}</span>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group" id="department" @if(!empty($visitor) &&  $visitor->type == 'general') style="display: none" @endif >
                                        <label for="recipient-name" class="control-label">Field / Department * </label>
                                        <input type="text" @if(!empty($visitor))  @if($visitor->type == 'official') required @endif @endif id="department_field" name="department"
                                               @if(!empty($visitor)) value="{{$visitor->department}}" @else value="{{ old('department') }}"
                                               @endif class="form-control" id="recipient-name">
                                        <span class="error-font text-danger">{{ $errors->first('department')}}</span>
                                    </div>
                                </div>
                          </div>
                          
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Designation</label>
                                  <input type="text" name="designition"
                                         @if(!empty($visitor)) value="{{$visitor->designition}}" @else value="{{ old('designition') }}"
                                         @endif class="form-control" id="recipient-name">
                                  <span class="error-font text-danger">{{ $errors->first('designition')}}</span>
                              </div>
                            </div>
                          </div>
                          
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="recipient-name" class="control-label">Work Description * </label>
                                  @if(!empty($visitor))
                                      <textarea name="description" class="form-control" id="" cols="30"
                                                rows="5">{!! $visitor->work_description !!}</textarea>
                                      <span class="error-font text-danger">{{ $errors->first('description')}}</span>
                                  @else
                                      <textarea name="description" class="form-control" id="" cols="30"
                                                rows="5">{{ old('description') }}</textarea>
                                      <span class="error-font text-danger">{{ $errors->first('description')}}</span>
                                  @endif
                              </div>
                            </div>
                          </div>
                          
                          
                        </div>

                        <div class="col-md-12">
                          <div class="box-footer text-center">
                              <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                        </div>

                        {!! Form::close() !!}

                      </div>
                    </div>
                    @if(!empty(session('type')) && session('type') == 'official')
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Tracking Details</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;font-size: 20px">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Name:  {{session('name')}}</label>

                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Tracking ID:  {{session('track_id')}}</label>

                                                  </div>

                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                   @endif
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });
    </script>
    <script>
        $( "#type" ).change(function() {
           var val = $(this).val();
             if(val == 'official'){
                 $('#department').show();
                 $('#department_field').attr('required',true);

             }else if(val == 'general'){
               $('#department').hide();
                 $('#department_field').attr('required',false);
             }
        });
    </script>
@endsection
@section('script')

@stop