@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tracking details</h3>
                        <div class="row">

                            <div class="col-md-6">
                                <div id="search-appointment" class="pull-right"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(array('url' => 'tracking/details')) !!}
                                <div class="col-md-3 col-sm-3">

                                    <div class="input-group date ">

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" readonly id="date_renger" class="form-control pull-right"   name="filter_date" >
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-3">

                                    <select name="type" class="form-control" id="">
                                        <option @if(!empty($type) && $type == 'In progress') selected @endif value="In progress">In progress</option>
                                        <option @if(!empty($type) && $type == 'Solve') selected @endif value="Solve">Solve</option>
                                        <option @if(!empty($type) && $type == 'Closed') selected @endif value="Closed">Closed</option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="submit" class="btn btn-primary">Filter</button>
                                </div>
                                {!! Form::close() !!}
                                <table id="table-visitor" style="width:100%" class="table table-bordered table-striped">
                                    <thead>
                                    <tr style="height: 45px">


                                        <th class="text-center">Visit Date</th>
                                        <th class="text-center">Tracking Id</th>
                                        <th class="text-center">Full Name</th>
                                        <th class="text-center">Contact</th>
                                        <th class="text-center">Description</th>
                                        <th class="text-center">Open from</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($visitors as $visitor)
                                        <tr style="height: 35px">
                                            <td class="text-center"> {{date('d-m-Y', strtotime($visitor->date_of_visit))}}</td>
                                            <td class="text-center"> {{$visitor->tracking_id}}</td>
                                            <td class="text-center">{{$visitor->full_name}}</td>
                                            <td class="text-center">{{$visitor->contact_number}}</td>
                                            <td class="text-center">{{$visitor->work_description}}</td>
                                            <?php
                                            $now = time(); // or your date as well
                                            $your_date = strtotime($visitor->created_at);
                                            $datediff = $now - $your_date;
                                            $day = round($datediff / (60 * 60 * 24));
                                            $text = '';
                                            if ($day == 0) {
                                                $text = "Today";
                                            } elseif ($day == 1) {
                                                $text = $day . ' day';
                                            } else {
                                                $text = $day . ' days';
                                            }
                                            ?>
                                            <td class="text-center"> {{$text}}</td>


                                            <td>{{$visitor->status}}</td>
                                            <td class="text-center">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('view/ticket').'?id='.$visitor->id}}">View</a></li>
                                                        <li><a href="{{url('edit/ticket').'?id='.$visitor->id}}">Update</a></li>
                                                        <li><a href="#" data-toggle="modal" data-target=".delete_{{$visitor->id}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- ========== DELETE MODEL START ========== -->
                                        <div class="modal fade delete_{{$visitor->id}}" id="delete-channel-modal "  role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-teal">
                                                <div class="modal-content">
                                                    <form id="form-delete-channel" role="form" method="POST" action="{{ url('/ticket/delete') }}" novalidate>
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h2 class="modal-title text-center color-skyblue">Ticket</h2>
                                                        </div>
                                                        <input type="hidden" name="id" value="{{$visitor->id}}">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="text-center">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <input type="hidden" id="channel-id" name="channel-id">
                                                                        <h4>Are you sure to delete this ticket?</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row">
                                                                <div class="col-md-8 col-md-offset-4">
                                                                    <input type="submit" class="btn btn-primary" value="Yes">
                                                                    <a href="/" class="btn btn-primary" data-dismiss="modal">No</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ========== DELETE MODEL END ========== -->

                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
    <script src="{{asset('/date-range/moment.js')}}" ></script>

    <script>
        $('#date_renger').daterangepicker({
            locale: {
              format: 'DD/MM/YYYY'
            },
            "startDate": "{{$start_date}}",
            "endDate": "{{$end_date}}"
        }, function(start, end, label) {
             console.log("New date range selected: ' + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY') + ' (predefined range: ' + label + ')");
        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('#table-visitor').dataTable( {
                "bLengthChange": false,
                "iDisplayLength": 20,
                "infoEmpty": "<center><div class='text-info'><br>No visitor available</div></center>",
                "oLanguage": {
                    "sEmptyTable":"<center><div class='text-info'><br>No visitor available</div></center>",
                    "sSearch": "",
                    "oPaginate": {
                        "sNext": '>',
                        "sLast": '>|',
                        "sFirst": '|<',
                        "sPrevious": '<'
                    }
                },
                "bSort" : false
            });
            $('.dataTables_filter input').attr("placeholder", "Search");
            $('.dataTables_filter input').removeClass("input-sm");
            $('.dataTables_filter input').addClass("form-control");
            $("#table-channel_info").detach().appendTo('#page-link-wrapper');
            $("#table-channel_paginate").detach().appendTo('#page-link-wrapper');
            $("#table-channel_filter").detach().appendTo('#search-channel');

            //Delete channel
            $(document).on('click', '.delete-channel-link', function(ev) {
                ev.preventDefault();
                var channel_id = $(this).data('channel-id');
                $('#channel-id').val(channel_id);
                $('#delete-channel-modal').modal('show');
            });
        });
    </script>
@endsection