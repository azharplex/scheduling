@extends('layouts.admin_header')
@section('content')


    <section class="content channel-container">
        <div class="row ">
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Details</h3>
                    </div>
                    <form id="form-add-channel" role="form" method="POST" action="{{ url('/notification/save') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            @if(!empty($notification))
                                <input type="hidden" name="id" value="{{$notification->id}}">
                                @endif
                            <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="channel">Name</label>
                                      <input type="text" class="form-control" id="channel" placeholder="Enter name" @if(!empty($notification)) value="{{$notification->name}}" @else value="{{ old('name')}}" @endif name="name" >
                                      <span class="error-font text-danger">{{ $errors->first('name')}}</span>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="channel">Email</label>
                                      <input type="email" class="form-control" id="channel" placeholder="Enter email name" name="email" @if(!empty($notification)) value="{{$notification->email}}" @else value="{{ old('email')}}" @endif>
                                      <span class="error-font text-danger">{{ $errors->first('email')}}</span>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="channel">Number</label>
                                      <input type="number" class="form-control" id="channel" placeholder="Enter number " @if(!empty($notification)) value="{{$notification->number}}" @else value="{{ old('email')}}" @endif name="number">
                                      <span class="error-font text-danger">{{ $errors->first('number')}}</span>
                                  </div>
                                </div>
                                 @if(empty($notification))
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="channel">Password</label>

                                        <input type="password" class="form-control"  id="channel" placeholder="Enter Password " @if(!empty($notification)) value="" @else value="{{ old('password')}}" @endif name="password">
                                        <span class="error-font text-danger">{{ $errors->first('password')}}</span>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <div class="box-footer text-center">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
          
            @if(!empty($notification))  
            @if(Auth::user()->id == $notification->id)
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Update Password</h3>
                    </div>
                    <form id="form-add-channel" role="form" method="POST" action="{{ url('/notification/password') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            @if(!empty($notification))
                                <input type="hidden" name="id" value="{{$notification->id}}">
                                @endif
                            <div class="row"> 
                                <div class="col-md-12"> 
                                  <div class="form-group">
                                      <label for="channel">Current Password</label>

                                      <input type="password" class="form-control"  id="channel" placeholder="Enter Current Password "  name="current-password">
                                      <span class="error-font text-danger">{{ $errors->first('current-password')}}</span>
                                  </div>
                              </div>
                              <div class="col-md-12"> 
                                  <div class="form-group">
                                      <label for="channel">Password</label>

                                      <input type="password" class="form-control"  id="channel" placeholder="Enter Password "  name="password">
                                      <span class="error-font text-danger">{{ $errors->first('password')}}</span>
                                  </div>
                              </div>
                             <div class="col-md-12"> 
                                  <div class="form-group">
                                      <label for="channel">Confirm Password</label>

                                      <input type="password" class="form-control"  id="channel" placeholder="Enter Password "  name="password_confirmation">
                                      <span class="error-font text-danger">{{ $errors->first('password_confirmation')}}</span>
                                  </div>
                                 
                              </div>
                               <div class="col-md-12"> 
                                  <div class="form-group">
                                   <div class="box-footer text-center">
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                  
                                  </div>
                              </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
            @endif
            @endif
        </div>
    </section>
@stop