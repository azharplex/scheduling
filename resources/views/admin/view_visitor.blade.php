{{--{{dd($visitor)}}--}}

@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Visitor Details</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><label style="width:100px; font-weight: 500;">NAME </label><b>: {{$visitor->full_name}}</b></h4>
                            </div>
                            <div class="col-md-12">
                                <h4><label style="width:100px; font-weight: 500;">STATUS </label><b>: {{$visitor->status}}</b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <table id="table-visitor" style="width:100%" class="table table-bordered table-striped">

                                    <tbody>



                                    <tr>
                                        <th class="text-left">Type</th>
                                        <td class="text-left">{{$visitor->type}}</td>

                                    </tr>
                                    <tr>
                                        <th class="text-left">Date of visit</th>
                                        <td class="text-left">{{$visitor->date_of_visit}}</td>
                                    </tr>
                                    @if($visitor->type == 'official')
                                    <tr>
                                        <th class="text-left">Tracking id</th>
                                        <td class="text-left">{{$visitor->tracking_id}}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th class="text-left">Email</th>
                                        <td class="text-left">{{$visitor->email}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Contact number</th>
                                        <td class="text-left">{{$visitor->contact_number}}</td>
                                    </tr>
                                    @if($visitor->type == 'official')
                                    <tr>
                                        <th class="text-left">Department</th>
                                            <td class="text-left">{{$visitor->department}}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th class="text-left">Designation</th>
                                        <td class="text-left">{{$visitor->designition}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Purpose of visit</th>
                                        <td class="text-left">{{$visitor->purpose_of_visit}}</td>
                                    </tr>
                                    <tr>

                                    </tr>
                                    <th class="text-left">Description</th>
                                    <td class="text-left">{{$visitor->work_description}}</td>
                                    <tr>
                                    @if($visitor->type == 'official')
                                    <tr>
                                        <th class="text-left">Open Status</th>
                                        <?php
                                        $now = time(); // or your date as well
                                        $your_date = strtotime($visitor->created_at);
                                        $datediff = $now - $your_date;
                                        $day = round($datediff / (60 * 60 * 24));
                                        $text = '';
                                        if ($day == 0) {
                                            $text = "Today";
                                        } elseif ($day == 1) {
                                            $text = 'Open from '. $day . ' day';
                                        } else {
                                            $text = 'Open from '.$day . ' days';
                                        }
                                        ?>
                                        <td class="text-left"> {{$text}}</td>
                                    </tr>
                                        @endif

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection