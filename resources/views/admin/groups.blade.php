@extends('layouts.admin_header')
@section('content')
  <section class="content user-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Add Group</h3>
          </div>
          <div class="box-body">
						<form method="POST" action="/groups/add" >
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<input type="hidden" name="list_id" value="{{ $list_id }}" />
							<input type="hidden" name="group_id" value="{{ $interest_group_id }}" />
							<div class="row form-group">
								<div class="col-md-4 col-md-offset-4">
									<label>Name</label>
									<input name="name" class="form-control" required />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-md-offset-4 text-center">
									<button type="submit" class="btn btn-success">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Group</h3>
            <div id="search-user" class="pull-right"></div>
          </div>
          <div class="box-body">
						<table id="table-users" class="table table-bordered table-striped">
							<thead>
								<th>Name</th>
								<th>Subscribers</th>
								<th class="text-center">Action</th>
							</thead>
							<tbody>
								@foreach($groups as $group_id => $group)
									<tr>
										<td>{{ $group }}</td>
										<td>{{ isset($subscribers[$group_id]) ? $subscribers[$group_id] : '' }}</td>
										<td class="text-center">
											<a href="/groups/edit?id={{ $group_id }}&group_id={{ $interest_group_id }}&list_id={{ $list_id }}&name={{ $group }}" class="btn btn-warning btn-xs">Edit</a>
											<a class="btn btn-danger btn-xs delete-link" data-id="{{ $group_id }}" data-group-id="{{ $interest_group_id }}" data-list-id="{{ $list_id }}" >Delete</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
            <div class="row">
              <div id="page-link-wrapper" class="col-md-12 text-center"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>  
  </section>
  
	<div class="modal fade" id="confirm" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title text-center">Confirmation</h4>
        </div>
        <div class="modal-body">
          Are you sure?
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-primary" id="yes">Yes</button>
          <button type="button" data-dismiss="modal" class="btn">NO</button>
        </div>
			</div>
		</div>
	</div>
	
  <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
  <script src="{{asset('/date-range/moment.js')}}" ></script>
  <script type="text/javascript">
		$(function(){
			$(document).on('click', '.delete-link', function(e){
				e.preventDefault();
				var id = $(this).data('id');
				var list_id = $(this).data('list-id');
				var group_id = $(this).data('group-id');
				$('.modal-body').text('');
				$('.modal-body').text('Are you sure you want to delete? ');
				$('#confirm').modal()
					.one('click', '#yes', function() {
						window.location.href = '/groups/delete?id='+id+'&list_id='+list_id+'&group_id='+group_id
					});
			});
			
			$('#table-users').dataTable( {
				"bLengthChange": false,
				"iDisplayLength": 15,
				"infoEmpty": "<center><div class='text-info'><br>No user available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No user available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : false
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			$("#table-users_info").detach().appendTo('#page-link-wrapper');
			$("#table-users_paginate").detach().appendTo('#page-link-wrapper');
			$("#table-users_filter").detach().appendTo('#search-user');

      $('.filter').on('click',function(){
        var filter_date = $(".date_renger").val();     
        var type = $('.select-type').val();     
        window.location.href = '/wishes/users?type='+type+'&filter_date='+filter_date;
      }); 
		});
	</script>
@endsection