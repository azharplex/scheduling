@extends('layouts.admin_header')
@section('content')
  <section class="content user-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Edit Group</h3>
          </div>
          <div class="box-body">
						<form method="POST" action="/groups/edit" >
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<input type="hidden" name="id" value="{{ $id }}" />
							<input type="hidden" name="list_id" value="{{ $list_id }}" />
							<input type="hidden" name="group_id" value="{{ $interest_group_id }}" />
							<div class="row form-group">
								<div class="col-md-4 col-md-offset-4">
									<label>Name</label>
									<input name="name" class="form-control" value="{{ $name }}" required />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-md-offset-4 text-center">
									<button type="submit" class="btn btn-success">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection