<html>
    <head>
    </head>
    <body>
        <table align="center" width="100%" height="100%" style="background-color:#f2f2f2">
            <tbody>
                <tr>
                    <td align="center" valign="top" style="padding:20px">
                        <table width="600">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                  <div style="background-color:#F39C12;color:#FFFFFF;height:10px;width:100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                   <a href="{{ Config('app.url') }}" target="_blank" style="color:#FFFFFF;text-decoration: none;"><h1 style="padding-left:28px; padding-top:10px;"></h1></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="background-color:#FFFFFF">
                                                        <table style="width:100%">
                                                            <tr>
                                                                <td style="padding:30px 30px 20px 30px">
                                                                    <p style="font-family:Roboto-Regular,Open Sans,Helvetica, Arial, sans-serif">Hello Sir,
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p style="padding-left:30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif;font-weight:600;">Tomorrows Appointment List
                                                                    </p> 
                                                                    <ul>
                                                                      @if(count($appointments) > 0)
                                                                        @foreach($appointments as $appointment)
                                                                          <li style="line-height:2;">{{ $appointment->full_name }} / {{ $appointment->contact_number }} / {{$appointment->email }} at <b>{{ date('h:i A', strtotime($appointment->appointment_time) )}}</b></li>
                                                                        @endforeach
                                                                      @else
                                                                          No Appointments For Tomorrow
                                                                      @endif
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                        </table>                                                   
                                                        
                                                    </div></br></br>
                                                    <div style="background-color:#F39C12;color:#FFFFFF;height:5px;width:100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                   <a href="{{ Config('app.url') }}" target="_blank" style="color:#FFFFFF;text-decoration: none;"><h1 style="padding-left:28px; padding-top:10px;"></h1></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>