<html>
    <head>
    </head>
    <body>
        <table align="center" width="100%" height="100%" style="background-color:#f2f2f2">
            <tbody>
                <tr>
                    <td align="center" valign="top" style="padding:20px">
                        <table width="600">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                  <div style="background-color:#F39C12;color:#FFFFFF;height:10px;width:100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                   <a href="{{ Config('app.url') }}" target="_blank" style="color:#FFFFFF;text-decoration: none;"><h1 style="padding-left:28px; padding-top:10px;"></h1></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="background-color:#FFFFFF">
                                                        <table style="width:100%">
                                                            <tr>
                                                                <td style="padding:30px 30px 20px 30px">
                                                                    <p style="font-family:Roboto-Regular,Open Sans,Helvetica, Arial, sans-serif">Hello Sir,
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p style="padding-left:30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif;font-weight:600;">Todays Birthday List
                                                                    </p> 
                                                                    <ul>
                                                                      @if(count($birthdays) > 0)
                                                                        @foreach($birthdays as $birthday)
                                                                          <li style="line-height:2;">{{ $birthday->full_name }}</li>
                                                                        @endforeach
                                                                      @else
                                                                          No Birthdays For Today
                                                                      @endif
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                              <tr>                                                              <td>
                                                                <hr style="padding-left:10%;padding-right:10%;color:#EEE;"> 
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p style="padding-left:30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif;font-weight:600;">Todays Anniversary List
                                                                    </p> 
                                                                 
                                                                    <ul>  
                                                                      @if(count($anniversaries) > 0)
                                                                        @foreach($anniversaries as $anniversary)
                                                                          <li style="line-height:2;">{{ $anniversary->full_name }}</li>
                                                                        @endforeach
                                                                      @else
                                                                        No Anniversaries For Today
                                                                      @endif
                                                                    </ul>
                                                                    
                                                                </td>                                                                
                                                               
                                                            </tr>
                                                        </table>                                                   
                                                        
                                                    </div></br></br>
                                                    <div style="background-color:#F39C12;color:#FFFFFF;height:5px;width:100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                   <a href="{{ Config('app.url') }}" target="_blank" style="color:#FFFFFF;text-decoration: none;"><h1 style="padding-left:28px; padding-top:10px;"></h1></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>