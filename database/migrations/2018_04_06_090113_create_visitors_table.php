<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('otp_id');
            $table->string('status')->default('In progress');
            $table->text('full_name');
            $table->string('type');
            $table->text('date_of_visit');
            $table->integer('tracking_id');
            $table->string('contact_number');
            $table->string('email');
            $table->string('department')->nullable();
            $table->string('designition');
            $table->string('purpose_of_visit');
            $table->longText('work_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visitors');
    }
}
