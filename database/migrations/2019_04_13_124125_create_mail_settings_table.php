<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailSettingsTable extends Migration
{
	public function up() {
		Schema::create('email_settings', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->text('value');
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('email_settings');
	}
}
