# Scheduling

Cron Commands 

  - To send sms and emails on todays wishes
  - To send sms and emails on tomorrows wishes
  - To send sms and emails on todays appiontment
  - To send sms and emails on tomottows appiontment

### Commands

Send SMS & Emails Todays Wishes:
```sh
php artisan sendtodaywishes
```

Send SMS & Emails Tomorrows Wishes:
```sh
php artisan sendtomorrowwishes
```
Send SMS & Emails Todays Appointments:
```sh
php artisan sendtodayappointmentlist
```
Send SMS & Emails Tomorrows Appointments:
```sh
php artisan sendtomorrowappointmentlist
```
Send SMS & Emails All Users on Birthday and Anniversary:
```sh
php artisan sendwishes
```

**Pleximus!**